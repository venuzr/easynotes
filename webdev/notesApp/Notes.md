#Testing using Testacular and integration with WebStorm
http://www.youtube.com/watch?v=MVw8N3hTfCI

# Javascript design patterns meetup notes
## References

> [Yahoo UI design patterns](http://developer.yahoo.com/ypatterns/) library

Function declaration vs function expressions.  In latter, assign function expression to a variable and invoke on the variable
In function declaration, functions are hoisted to the top of their corresponding scope.  For function expression, no hoisting takes place

There are many ways to do this in javascript and many of the *principles* are __inconsistent__ with each other

## Patterns discussed
###Design patterns
- Singleton
- Observer
- Publisher/subscriber

#### Code pattern

For a self executing block which is executed as soon as it is defined

1. Jquery/Underscore style
`
    (function f1() {
    ...
    })();
`
2. Douglas Crockford style
`
    (function f1() {
    ...
    }());`


## Ace editor references
https://groups.google.com/forum/?fromgroups=#!searchin/ace-discuss/change$20theme/ace-discuss/puG8bpH028Q/drUTChpF6AQJ


## D3 References
##Effects
###Distortion effect
https://groups.google.com/forum/?fromgroups=#!searchin/d3-js/tree/d3-js/fVIt9x1yMNU/Vy1F_vQ6vK8J
http://enja.org/code/advd3/warp/
https://github.com/enjalot/adventures_in_d3/tree/master/warp


### Adding hyperlinks
http://stackoverflow.com/questions/10588490/d3-js-tree-structure-text-links

### Words in different angles
https://github.com/jasondavies/d3-cloud

### Powerank style
http://thepowerrank.com/visual/NCAA_Tournament_Predictions

### Distance scaling of lines
http://stackoverflow.com/questions/10177448/insert-nodes-on-path-with-changing-distance-based-on-their-count

### Zoom behaviour
https://github.com/mbostock/d3/wiki/Zoom-Behavior

### Adding images https://groups.google.com/forum/#!topic/d3-js/fL8_1BLrCyo

Arborjs.org
draculajs http://www.graphdracula.net/
http://thejit.org/demos/
http://processingjs.org/exhibition/


