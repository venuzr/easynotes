## TODO :
Get the minification of all the files working properly https://github.com/CaryLandholt/AngularFun
Use Intellij live templates from https://github.com/angularjs-livetpls/angularjs-webstorm-livetpls

## External tools
### Known issues
- Markdown.Editor.js causes errors when trying to run 'yeoman build'.  So currently excluding

## Execution
Run 'yeoman server' to run the application

## Testing
### Unit testing
- First run the command 'python -m SimpleHTTPServer 3502 &'.  The proxy is setup to serve the fixtures from this port
- Run 'yeoman test'
#### For continuous integration testing
testacular start testacular.conf.js


### Integration/e2e testing
- Goto http://localhost:3501/scripts/e2e/runner.html
- Run ./config/e2e-test.sh (Ensure that the message shows atleast 1 test in Executed 1 of 1 SUCCESS )

In case of trouble, look at [angular-seed](https://github.com/angular/angular-seed) project for reference
https://github.com/angular/angular-seed/pull/28

#### For continuous integration testing
testacular start config/testacular-e2e.config.js

###Reference articles

-Yeoman
http://net.tutsplus.com/tutorials/tools-and-tips/say-yo-to-yeoman/

-AngularJS + RequireJS
https://github.com/mmrath/angular-seed/tree/master/app


#### Errors to watch out for
- If there are too many files under the scripts folder, then 'yeoman server' command will blow up


http://www.scribd.com/doc/79347238/Datenvisualisierung-mit-WebGL-Three-js
https://github.com/davidpiegza/Graph-Visualization
https://www.youtube.com/watch?v=R4H2B4HIJzo


