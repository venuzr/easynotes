# Scala classes

Multiple expression evaluation strategies possible

## Call by name and call-by-value.
-Call by value has advantage of needing lesser number of steps as it is precomputed
-Call by name has advantage that some evaluations need not be performed at all.
There can be functions which end (avoid infinite loop) with call by name but loop endlessly
with call by value
-Scala uses call by value by default
-Can force call by using :=

<pre><code>
def fn(x: Double, y: => Double) := 1
</code></pre>

-The second argument is call by name and first is call by value

Types of fonts iABC
http://informationarchitects.net/blog/responsive-typography/


@tailrec
    def straightIterate(outerChars: List[Char], innerChars: List[Char]): Boolean = {
      if (!outerChars.isEmpty) println("outer head" + outerChars.head)

      if (innerChars == null && outerChars.isEmpty) { println( "1"); true}
      else if (innerChars == null && outerChars.head == ')') { println( "2"); false}
      else if (innerChars == null && outerChars.head == '(') { println( "3"); straightIterate(outerChars, outerChars.tail)}
      else if (innerChars != null){
        if (innerChars.isEmpty) false
        else if (innerChars.head == ')'){
          if(outerChars.head == '(') { println("4" + outerChars.head);true}
          else { println("4.5" + outerChars.head + " " + innerChars.head); false}
        } else { println("5" + outerChars.head + " " + innerChars.head); straightIterate(outerChars, innerChars.tail)}
      }

      else {
        println("6" + outerChars.head);
        //val temp : List[Char] = List()
        val temp = null

        straightIterate(outerChars.tail, temp)
      }
    }



 else if (innerChars != null){
        if (innerChars.isEmpty) false
        else
        if (innerChars.head == '('){
          println("4.2" + outerChars.head + " " + innerChars.head);
          straightIterate(innerChars, innerChars.tail)
        }

        else if (innerChars.head == ')'){
          if(outerChars.head == '(') { println("4" + outerChars.head + innerChars.head);straightIterate(innerChars.tail, null)}
          else { println("4.5" + outerChars.head + " " + innerChars.head); false}
        } else { println("5" + outerChars.head + " " + innerChars.head); straightIterate(outerChars, innerChars.tail)}
      }


def innerCountChange(innerMoney: Int, innerCoins: List[Int]): Int = {
      /*
      if (innerMoney == 0) 1
      else if (innerMoney < 0) 0
      else if (innerCoins.isEmpty) 0
      else if (innerCoins.head < innerMoney) {
        print(innerCoins.head)
        innerCountChange(innerMoney - innerCoins.head, innerCoins)
      }
      else innerCountChange(innerMoney, innerCoins.tail)
      */
    //}
    //innerCountChange(money, coins)
