# Asia trip
## Taiwan leg
### Taipei
#### Night market
#### Wedding
### Taroka gorge

##Thailand leg
### Bangkok
#### Leisure activities
> (http://www.bangkok.com/top10-short-tours.htm)
> (http://www.tripadvisor.com/Attractions-g293916-Activities-Bangkok.html)

Here is how you embed images
[![__Foo__ bar thing](http://www.google.com.au/images/nav_logo7.png)](http://google.com.au/)

#### Temples
1.Wat Arun

2.Wat Pho

3.Wat Saket

4.Wat Traimit

#### Other
1.Floating market

2.Food tasting

3.Massage

4.Checkout museum

5.Checkout architecture

#### Beaches and water activities
1.Hua Hin Beach
-Water activities

2.[Ang Thong](http://www.tripadvisor.co.uk/Attraction_Review-g293918-d450829-Reviews-Angthong_National_Marine_Park-Koh_Samui_Surat_Thani_Province.html)
(2 hrs from Bangkok)

3. Pattaya

### Chiang Mai
#### Leisure activities
1.Cooking class

2.Elephant rides

3.Culture/temples

4.Massage / Spa

#### Outdoor activities - National parks  / Jungle trekking
1.__Doi Inthanon National Park__ around a hour from Chiang Mai (Mae Ya waterfall)
> (http://www.thailanddiscounthotel.com/guide/hiking-in-thailands-national-parks-treasures-of-chiang-mai.php)
> (http://www.tripadvisor.com/Attraction_Review-g293917-d455307-Reviews-Doi_Inthananon-Chiang_Mai.html)
> Best to rent a motorbike

*Can get accomodation in the national park

2.__Doi Luang Chiang Dao__ (around 3 hrours from Chiang Mai) is a limestone mountain in the Chiang Dao Wildlife Reserve area,
> (http://www.7thaiwonders.com/doi-luang-chiang-dao.html)


http://www.tripadvisor.com/Attraction_Review-g293917-d1021770-Reviews-Flight_of_the_Gibbon_Treetop_Adventure-Chiang_Mai.html
http://www.squidoo.com/Doi_Inthanon_Thailand
http://www.lonelyplanet.com/thailand/chiang-mai-province/chiang-dao


### South Thailand
#### Locations
##### Koh Phi Phi
* Too crowded
* Beautiful

##### Ko Samui
* Accessible by direct flight from Chiang Mai
* Very crowded
* Some of the best beaches

##### Phuket
* Quite large.  Some beaches are too commercial and crowded while others are not

##### Krabi
    1.Beach    http://www.lonelyplanet.com/thorntree/thread.jspa?threadID=1913773
        1.[Railay Beach](http://gothailand.about.com/od/topdestinations/p/raileybeach.htm)
        - Lots of activities available
    2. Ao Nang

##### Ko Chang
* Doesnt seem very promising
* On eastern side of thailand and too far away

#### Seaside resort
##### Just beaches

####  Beaches with national parks
>http://gothailand.about.com/od/whattoseedo/tp/Thailands-Beach-National-Parks.htm
>http://gothailand.about.com/od/topbeachdestinations/tp/topfiveotherbeaches.htm
>http://www.thailandbeachvillas.com/information-details.php?title=hua%20hin%20beach%20and%20water%20sports
>http://gothailand.about.com/od/topdestinations/tp/toptenbeaches.htm

## India
###Transit
-New delhi to Agra - 2 to 3 hrs
-Jaipur to New delhi - 5 hrs (http://goindia.about.com/od/populartrainroutes/qt/delhi-jaipur-train.htm)
    -2015 Ajmer Shatabdi (cost around $10)
    -Flying will take around 1 hr and cost around $75
-Agra to Jaipur - 5 hrs by train and around 50 minutes by flight

### New Delhi
-Presidential palace
-Red Fort (closed on Monday)
-Flea market
-[Qutab Minar](http://www.tripadvisor.com/Attraction_Review-g304551-d311626-Reviews-Qutab_Minar-New_Delhi_National_Capital_Territory_of_Delhi.html)
-[Jantar Manta](http://en.wikipedia.org/wiki/Jantar_Mantar_(Delhi))
-[Bahai Lotus temple](http://www.tripadvisor.com/Attraction_Review-g297602-d311659-Reviews-Bahai_Lotus_Temple-National_Capital_Territory_of_Delhi.html)

#### Agra
- Goto Agra to see Taj Mahal
- Take the Shatabdi express to http://goindia.about.com/od/populartrainroutes/qt/delhi-agra-trains.htm

New delhi -> Agra on December 24th ( Monday)
    - 12002 Bhopal Shatabdi ( 2 hrs) 6:15 am -> 8:12 am
    - 2280 Taj Express Superfast ( 3 hrs)
12002	Bhopal Shatabdi	New Delhi	 06:15	Agra Cantt	 08:12	Y	Y	Y	Y	N	Y	Y
12280	Taj Express	H Nizamuddin	 07:10	Agra Cantt	 10:07	Y	Y	Y	Y	Y	Y	Y


Agra to New delhi(3 hrs)                                                M
12645	Nizamuddin Express	Agra Cantt	 14:05	H Nizamuddin	 17:00	Y	N	N	N	N	N	N
12807	Samta Express	Agra Cantt	 13:45	    H Nizamuddin	 16:40	Y	N	Y	Y	Y	N	Y
12279	Taj Express	Agra Cantt	 18:55	        H Nizamuddin	 22:00	Y	Y	Y	Y	Y	Y	Y
18237	Chattisgarh Express	Agra Cantt	 16:10	H Nizamuddin	 19:45	Y	Y	Y	Y	Y	Y	Y

#### [Jaipur](http://www.jaipurtravel.com/the_pinkcity_walk.htm)
- Take agra to Jaipur trains (4 - 5 hrs)
- Should we make this an overnight trip
##### [Sights](http://www.rajasthantourism.biz/jaipurguide/jaipur-sightseeing.htm)
-[City Palace](http://www.holidayiq.com/City-Palace-Jaipur-Sightseeing-383-8218.html)
-Hawa Mahal
-Amber fort
-[Jal Mahal](http://www.rbstravels.com/places-to-visit-jaipur.htm)
-Jantal Mantar
-Can buy cool shoes for Yuchen  (http://www.youtube.com/watch?v=WYAYKcyarXg)


### Bangalore
### Mysore
### Mangalore
### Kerala

