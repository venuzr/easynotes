# Todo

## Get later
### icons
1.Readonly
2.Preview with image
3.Wordwrap icon
4.Icons for H1, H2 so on
5.Settings icon



### Do later
#### Home page top nav
1.Search box doesnt look right.  Fix as per www.pivory.com.
2.Ensure that Following are working
    1.File open
    2.File save
    3.Export(in different format(html/pdf).
        1.Html - high prioriy
        2.PDF - Low priority

#### Preview pane
1.Add dynamic scrolling(horizontal atleast if not vertical)

#### Editor
1.Add dynamic scrolling(horizontal atleast if not vertical)

## Features
### Improvements to markdown
1.Node value
2.Parent to child relationship
3.Child to parent relationship(is this necessary)
4.Classification(For putting it in different subjects)
5.Tagging/Labelling (Searchable)

### Graph tool
1.Ability to pan and zoom to different sections of the graph
2.Graph should change based on which area is being read in preview mode
3.

### Search results
1.Match by tag
2.Limit results by classification
3.Search text itself

Check this out
http://oscargodson.github.com/EpicEditor/


Uploading files
http://plnkr.co/edit/yCKaKH?p=explore

https://github.com/blueimp/jQuery-File-Upload
https://github.com/blueimp/jQuery-File-Upload/wiki/Basic-plugin

Special notation to indicate images: get images from the web to help draw the diagram
^^ ...^^



TODO

FileSystemUtils.saveFile function : create flag needs to be true for now to force download

Cool angular JS example with interaction between directive and controller
http://jsfiddle.net/simpulton/5gWjY/
AngularJS - broadcast scenario
http://jsfiddle.net/simpulton/GeAAB/
Example of scope.$emit
http://jsfiddle.net/mhevery/xaqNZ/21/


d3 test editor
http://enjalot.com/tributary/3918930/
Force field radial graph http://bl.ocks.org/2879486
xkcd style graphs.  Would be very interesting to get the handrwiting style jitter effect for the maps.
    http://dan.iel.fm/xkcd/
    http://bl.ocks.org/3914862
    http://jakevdp.github.com/blog/2012/10/07/xkcd-style-plots-in-matplotlib/
    http://webhome.cs.uvic.ca/~blob/publications/zainabcag.pdf
    http://mathematica.stackexchange.com/questions/11350/xkcd-style-graphs
    http://bl.ocks.org/3815828

    Berkeley course on visualization http://blogs.ischool.berkeley.edu/i247s12/syllabus/lecture-1-introduction/
    http://blog.visual.ly/10-things-you-can-learn-from-the-new-york-times-data-visualizations/





Ace editor
https://github.com/ajaxorg/ace/blob/master/lib/ace/mode/javascript.js

Example of how to create workers to constantly sync


http://benalman.com/projects/jquery-dotimeout-plugin/

May need to look into this later:
https://github.com/ajaxorg/ace/issues/600



