/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 10/6/12
 * Time: 3:06 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 9/8/12
 * Time: 1:09 PM
 * To change this template use File | Settings | File Templates.
 */
/********Nested namespace defintion *******/
var mm = mm || {
    /********Namespaces *****/
    layout:{},
    core:{
        visualizations :{},
        parser : {}
    },
    /***Global variables ***/
    profile:{
        mode:"ace/mode/markdown"
    },
    cookieKeys : {
        viewMode : "mm.ck.vmode",
        theme : "mm.ck.theme",
        currentOpenFile : "mm.ck.cof",
        recentFiles : "mm.ck.rf",
        isNewFile : "mm.ck.newfile",
        currentContents : "mm.ck.contents",
        graphStyle : "mm.ck.graphStyle"
    },
    utility:{},
    notes:{
        editor:{
            isInitialized:false,
            defaultDelay:100,
            updateEvent:"OnEditorChange"
        },
        defaults:{
            updateEvent:"OnEditorChange",
            editorTheme:"ace/theme/chrome",
            displayMode : "p",
            graphStyle : "ddg",
            parser:{
                nodeTag:"$$", /**This is for what appears inside the node **/
                p2CrelationTag:"@@", /***This is for c2p relationships ***/
                c2PrelationTag:"^^", /***This is for c2p relationships ***/
                startTag:"{{", //Do I need this
                endTag:"}}",  //Do I need this
                leafTag:"@@", //Do I need this
                startContext:"[[", /***This is for mapping regions ***/
                endContext:"]]", /***This is for mapping end of regions ***/
                textStartIndex:0,
                textEndIndex:15,
                delimiter : "$$$"
            },
            debounce : {           //All times in milliseconds
                resize : 100,
                updateEditor : 300
            },
            cache :{
                expiryMilliSeconds : 1000 * 60 * 20 // 20 minutes
            }
        },
        updateEventTrigger:{
            editor:"OnEditorChange",
            keyUp:"OnKeyUp",
            refresh:"onLoad",
            changeTheme:"onChangeTheme"
        },
        displayMode:{
            editing:"e",
            reading:"r",
            readingWithMindMap:"m",
            preview:"p"
        },
        graphStyle:{
            dendogram : "ddg",
            dendogramInteractive : "ddgI",
            dendogramInteractiveNonCircular : "ddgINC",
            forceRadial : "fR",
            radial: "radial"
        }
    }


};




