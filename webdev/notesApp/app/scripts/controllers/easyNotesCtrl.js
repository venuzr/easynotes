'use strict';


notesApp
    .controller('fileuploadCtrl', ['$scope', 'noteServicesLibrary',
        function ($scope, noteServicesLibrary) {


            /*Above notation of using $scope twice is used to avoid issues during minification */
            $scope.setViewingModeAndEditor = function (valueContents) {
                noteServicesLibrary.setViewingModeAndEditor($scope, valueContents);
            }
            $scope.checkCookieStates = function (currentOpenFile, chosenFileName, currentViewingMode, recentFiles,
                                                 isNewFile, fileContents) {
                var isDiff = false;

                if (currentOpenFile !== undefined &&
                    currentOpenFile === chosenFileName) {
                    ///Do nothing here as same file
                } else {
                    isDiff = true;
                    /***Different file is loaded ***/
                    //closureSetEditorContents(fileContents, currentViewingMode);
                    currentOpenFile = chosenFileName;
                    if (recentFiles === undefined || recentFiles === null) {
                        recentFiles = chosenFileName;
                    } else {
                        var found = mm.core.StringUtils.searchForSubString(
                            recentFiles, chosenFileName, mm.notes.defaults.parser.delimiter);
                        if (found === false)
                            recentFiles += mm.notes.defaults.parser.delimiter + chosenFileName;
                    }
                    isNewFile = false;
                }
                return {
                    'isDiff' : isDiff,
                    'currentOpenFile':currentOpenFile,
                    'recentFiles':recentFiles,
                    'isNewFile':isNewFile
                };
            }
        }])
    .controller('MainCtrl', ['$scope', 'noteServicesLibrary',
        function ($scope,  noteServicesLibrary) {
            /*Above notation of using $scope twice is used to avoid issues during minification */
            $scope.setViewingModeAndEditor = function (valueContents) {
                noteServicesLibrary.setViewingModeAndEditor($scope, valueContents);
            }

            $scope.previewDisplay = false;
            $scope.mindMapDisplay = true;
            $scope.editorDisplay = true;
        
            $scope.toggleViewMode = function(mode) {
                if (mode === 'preview') {
                    $scope.previewDisplay = true;
                    $scope.mindMapDisplay = true;
                    $scope.editorDisplay = false;
                } else if (mode === 'edit') {
                    $scope.previewDisplay = true;
                    $scope.mindMapDisplay = false;
                    $scope.editorDisplay = true;
                } else {
                    $scope.previewDisplay = false;
                    $scope.mindMapDisplay = true;
                    $scope.editorDisplay = true;
                    
                }
            };


        }])
    .controller('fileopendialogCtrl', ['$scope', 'noteServicesLibrary',
        function ($scope,  noteServicesLibrary) {
            /*Above notation of using $scope twice is used to avoid issues during minification */



        }])
    .controller('markdownEditorCtrl', ['$scope',  'noteServicesLibrary',
        function ($scope,  noteServicesLibrary) {
            /*Above notation of using $scope twice is used to avoid issues during minification */
        
        }]);







