/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 10/18/12
 * Time: 10:53 AM
 * To change this template use File | Settings | File Templates.
 */

mm.core.cookieHelper = function () {
    function set(key, value, expiryMilliSeconds){
        $.jStorage.set(key,value);
        if (expiryMilliSeconds){
            $.jStorage.setTTL(key, expiryMilliSeconds);
        }
    }

    function get(key, defaultValue){
        return $.jStorage.get(key, defaultValue);
    }

    // Reveal public pointers to private functions and properties
    return {
        get : get,
        set : set
    };///End of Return

}();