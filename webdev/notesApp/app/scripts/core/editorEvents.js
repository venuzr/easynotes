/**
 * User: venuzr
 * Date: 10/9/12
 * Time: 1:16 AM
 * To change this template use File | Settings | File Templates.
 */

/***************************** Method declaration ********************/

mm.core.EditorEvents = function () {

    function setEditorContents(valueContents, currentViewingMode) {
        var editor = getEditor();
        editor.setValue(valueContents);
    }
    /***
     * Event to be fired when the user changes editor theme
     * @param event
     */
    function editorThemeSelectorChange(themeValue) {
        //var themeValue = $("select option:selected").val();
        changeTheme(themeValue.trim());
    }



    /***
     * Setup Ace editor settings and onChange event
     * @param themeVal  : Editor theme value
     */
    function setupEditor(themeVal) {
        var editor = getEditor();
        themeVal = themeVal || mm.core.cookieHelper.get(mm.cookieKeys.theme, mm.notes.defaults.editorTheme)
        if (themeVal) {
            editor.setTheme(themeVal);
            mm.core.cookieHelper.set(mm.cookieKeys.theme, themeVal, mm.notes.defaults.cache.expiryMilliSeconds)
        }
        editor.getSession().setMode(mm.profile.mode);
        editor.setShowPrintMargin(false);

        //editor.height("900px");
        return editor;


        //editorSession.setUseWrapMode(true);

        //editor.setHighlightActiveLine(false);

        //editorSession.setUseSoftTabs(true);
    }

    /****
     * 1) Determine if a change really happened to trigger an update
     * 2) Get the editor content and chain the event (this is needed in first time page load especially)
     * 3) Update preview/visualization
     *
     * @param themeVal  : Editor theme value
     */
    function changeTheme(themeVal) {
        if (mm.notes.editor.isInitialized !== false &&
            (mm.profile.theme === themeVal))
            return;   /*Ensure that update does not fire if the theme selected is same as current one */

        setupEditor(themeVal);
        updatePreviewAndSettings(themeVal);
    }

    /**
     * Only update the preview when the editor contents are different from current contents.
     */
    function onEditorChange(force) {
        if (force === true ||
            ((mm.notes.editor.updateEvent === mm.notes.updateEventTrigger.editor))) {
                //console.log("Before updatePreviewAndSettings" + force)
                updatePreviewAndSettings(mm.core.cookieHelper.get(mm.cookieKeys.theme, mm.notes.defaults.editorTheme));
            return true;
        }
        return false;
    }


    function setViewingMode(currentViewingMode, shouldForce) {
        console.log("In setViewMode");
        //console.log("current viewing mode:" + currentViewingMode)

        var drawTree;
        if ((mm.core.EditorEvents.onEditorChange(shouldForce) === true)) { //Update occured
            drawTree = mm.core.parser.SimpleHtmlParser.parse($('#preview'));

            if(currentViewingMode === mm.notes.displayMode.preview)   {
                    drawVisualization(drawTree);
            }
        }

        switch(currentViewingMode){
            case mm.notes.displayMode.editing :
                console.log("In editing mode")
                //$("#preview").addClass('editorial_settings')
                $('#preview').html(mm.core.parser.SimpleHtmlParser.getCleanedHtml($('#preview').html()))

                togglePreview(false);
                break;
            case mm.notes.displayMode.readingWithMindMap :
                console.log("In readingWithMindMap mode")
                break;
            case mm.notes.displayMode.preview :
                drawVisualization(drawTree);


                break;
            default :
                console.log("In default setViewmode");
                togglePreview(false);
                break;

        }


    }

    /****Draw the mind map using the data tree ***/
    function drawVisualization(drawTree) {
        //mm.core.visualizations.ClusterRadialMindMap(drawTree);
        if (drawTree === undefined || drawTree[0] === undefined) {
             togglePreview(false);
        }
        else {
            mm.core.visualizations.TreeNonInteractive(drawTree);
            togglePreview(true);
        }
    }

    function togglePreview(toggle) {
        if (toggle) {
            $('#preview').hide();
            $('#previewWithMindMap').show();
        }
        else {
            $('#preview').show();
            $('#previewWithMindMap').hide();
        }
    }

    /****
     * Gets the user's theme and
     * 1) Updates the preview from the editor contents as it needs to be converted to HTML anyway.
     * 2) Depending on whether the preview is being displayed, figures out whether to draw the visualization
     * 3) Finally updates user preferences with new theme value
     * @param themeVal : Editor theme value
     */
    function updatePreviewAndSettings(themeVal) {
        _previewMode();
        /***
         * Sets user preferences.  TODO : Look into moving to some other option like cookies rather than
         * using global variables
         */
        function setUserPreferences() {
            /*************Update current settings ************/
            mm.notes.editor.isInitialized = true;
            if (themeVal !== undefined)
                mm.core.cookieHelper.set(mm.cookieKeys.theme, themeVal,
                    mm.notes.defaults.cache.expiryMilliSeconds);
            mm.core.cookieHelper.set(mm.cookieKeys.currentContents, _getEditorMarkdown(),
                    mm.notes.defaults.cache.expiryMilliSeconds);
        }
        setUserPreferences();
    }




    function getEditor() {
        return ace.edit("editor1");
    }
    /**
     * Private method
     * Gets the content from the editor and converts it into Html
     * @return : Html content
     */
    function _getEditorMarkdown() {
        var editor = setupEditor()

        var converter = new Markdown.Converter();
        var unmd = editor.getSession().getValue()
            , md = converter.makeHtml(unmd)
        return  md;

    }

     /**
     * Private method
     * Get the markdown from the editor, convert it to html and push it to the preview panel/div
     */
    function _previewMode() {
        var md = _getEditorMarkdown();
        $('#preview')
            .html('')// unnecessary?
            .html(md);
    }

    // Reveal public pointers to private functions and properties
    return {
        changeTheme:changeTheme,
        editorThemeSelectorChange:editorThemeSelectorChange,
        setEditorContents : setEditorContents,
        onEditorChange : onEditorChange,
        setViewingMode : setViewingMode,
        getEditor : getEditor
    };///End of Return

}();
