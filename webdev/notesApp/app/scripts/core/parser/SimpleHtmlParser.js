/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 9/17/12
 * Time: 10:59 AM
 * To change this template use File | Settings | File Templates.
 */

/********
 * A parser for parsing input HTML and creating the tree representation so it can be visualized
 * @return {* A tree representation of html}
 * @constructor
 */
mm.core.parser.SimpleHtmlParser = function () {
    /******Initialization code *********/
    var levelLookup = {};
    levelLookup["h1"] = 1;
    levelLookup["h2"] = 2;
    levelLookup["h3"] = 3;
    levelLookup["h4"] = 4;
    levelLookup["h5"] = 5;
    levelLookup["h6"] = 6;
    //levelLookup["p"] = 7;

    // Reveal public pointers to private functions and properties
    return {
        parse:parse,
        getCleanedHtml:getCleanedHtml
    };

    /**
     * Regex pattern from //http://www.javascriptkit.com/javatutors/re2.shtml
     * //Replace $$ or @@ or ^^ globally
     * @param text - Input text
     * @return {*} - text which has all custom tags removed
     */
    function getCleanedHtml(text){
        if(text) {
            text = text.replace(/[$]{2}|[@]{2}|[\^]{2}/g, "")
        }
        return text;
    }



    function parse(selectedElement) {


        var outputArray = new Array();
        //var selectedElement = $('#preview');
        var precedenceArr = ["h1", "h2", "h3", "h4", "h5", "h6"];
        var arr = new Array();
        arr[0] = selectedElement;
        /******Root of them all ****/

        /********End initialization code.  Actual work done here *****/
        var drawTree = _getPrecedenceTree(
            arr, precedenceArr, levelLookup, selectedElement, outputArray);
        return drawTree;
    }


    /**
     * Get a tree representation of all nodes for the DOM elements
     * @param arrayElements : All DOM elements
     * @param precedenceArr :prcedence array of selected DOM elements we are interested in
     * @param levelLookup : level values of each DOM element we are interested in
     * @param parentElement : selected parent DOM elements
     * @param outputArray : array containing all the output nodes (ending in tree form) for corresponding DOM elements
     * @return {* Tree containing all nodes corresponding to DOM elements}
     */
    function _getPrecedenceTree(arrayElements, precedenceArr, levelLookup, parentElement, outputArray) {

        if (parentElement.children !== undefined) {
            for (var outerLoop = 0; outerLoop < arrayElements.length; outerLoop++) {
                parentElement.children().each(function (index, value) {
                    var currState = mm.core.parser.NodeParser.getElement(value, levelLookup, index);
                    if (currState !== undefined)
                        outputArray.push(currState);
                });
            }
        }

        var dupCounterObj = {counter:1};
        _reArrangeTree(outputArray, dupCounterObj);


        return _.filter(outputArray, function (selectedElement) {
            return !selectedElement.duplicate
            /*Remove all elements from root array which have been marked as duplicate as they have been
             * accounted for in subtree
             */
        });
    }


    /*******************************************************************************************************************
     *
     * @param potentialParentNode : Root of potential subtree to be added
     * @param newChildNode : New child to be added
     *
     * @return {* Given a potentialParent which is root of the subtree in which the newChildNode has to be added
     * find the latest element in that subtree in which the newChild can be added}
     *****************************************************************************************************************/
    function _findLatestCorrespondingElement(potentialParentNode, newChildNode) {
        if (potentialParentNode.level + 1 !== newChildNode.level &&
            potentialParentNode.children !== null &&
            potentialParentNode.children.length > 0) {

            for (var childIndex = potentialParentNode.children.length - 1; childIndex >= 0; childIndex--) {
                var retVal = _findLatestCorrespondingElement(
                    potentialParentNode.children[childIndex], newChildNode);
                if (retVal !== undefined) return retVal;
            }
        }
        return potentialParentNode;

    }

    /****************************************************************************************************************
     * @param selectionArray : List of all nodes in array format
     * @param dupCounterObj : Counter of all processed nodes
     *
     * Given a list of all nodes in arry format, convert it into a tree in parent-child form.
     * For e.g., H1 is parent of H2 which is parent of H3 and so on
     *****************************************************************************************************************/
    function _reArrangeTree(selectionArray, dupCounterObj) {

        for (var outerLoop = 0; outerLoop < selectionArray.length &&
            dupCounterObj.counter < selectionArray.length; outerLoop++) {
            if (selectionArray[outerLoop].duplicate === false) { //Ensure that extra iterations of outer loop are avoided
                for (var innerLoop = outerLoop + 1; innerLoop < selectionArray.length; innerLoop++) {
                    if (selectionArray[innerLoop].level <= selectionArray[outerLoop].level) {
                        break;
                        /*Ensure that extra iterations of outer loop are avoided. If outerloop and innerLoop are
                         at same level then the innerLevel cannot be child of outerLoop element and hence can
                         stop processing outerLoop*/
                    }
                    else if (selectionArray[innerLoop].duplicate === false) { //process element if not already processed
                        var selectedParent = _findLatestCorrespondingElement(
                            selectionArray[outerLoop], selectionArray[innerLoop]);

                        _addChildNode(selectedParent, selectionArray[innerLoop], dupCounterObj);

                        /*******TODO: I don't this is this necessary as with dupCounter, this issue is solved?
                         * Need to verify this but if selected parent is not the same as outer parent, then can
                         * skip the current iteration of outer loop.  For now, check on level,
                         * Refactor to include id in future

                         if (selectedParent.level !== selectionArray[outerLoop].level)
                         break;
                         */
                    }
                }
            }
        }

    }

    /****
     * Given the selectedParent and childNode, add the childNode to the parentNode and ensure all other
     * values are setup/initialized correctly
     * @param selectedParent : parent node to add child
     * @param childNode
     * @param dupCounterObj : duplicate counter object
     */
    function _addChildNode(selectedParent, childNode, dupCounterObj) {
        if (!selectedParent.children)
            selectedParent.children = [];

        selectedParent.children.push(childNode);
        childNode.duplicate = true;
        dupCounterObj.counter += 1;
        childNode.parent = selectedParent;
    }
}();

