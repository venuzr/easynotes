/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 10/23/12
 * Time: 11:41 AM
 * To change this template use File | Settings | File Templates.
 */

mm.core.parser.NodeParser = function () {
    /******Initialization code *********/
    var levelLookup = {};
    levelLookup["h1"] = 1;
    levelLookup["h2"] = 2;
    levelLookup["h3"] = 3;
    levelLookup["h4"] = 4;
    levelLookup["h5"] = 5;
    levelLookup["h6"] = 6;
    //levelLookup["p"] = 7;


    // Reveal public pointers to private functions and properties
    return {
        getElement:getElement,
        getLookup : getLookup
    };

    /*** Only Used for testing purposes **/
    function getLookup(){
        return levelLookup;
    }

    /**
     * Given the selectedElement and the state precedence hash table, get the corresponding state node populated with
     * level and text.
     * Todo : Relationship value as well
     * @param selectedDOMElement : Selected DOM element
     * @param levelLookup : Precedence hash table
     * @return {* Node value corresponding to DOM element}
     */
    function getElement(selectedDOMElement, index) {
        var selectedLevel = levelLookup[selectedDOMElement.localName];
        /*localName is in lowerCase */
        if (selectedLevel === undefined) return undefined;

        var selectedText = _getSelectedText(selectedDOMElement);
        if (selectedText === undefined) return undefined;

        var currState = {
            name:selectedText.text,
            level:selectedLevel,
            children:null,
            duplicate:false,
            strength:1,
            p2CrelationShip:selectedText.p2c,
            c2PrelationShip:selectedText.c2p,
            id:index + 1, /*Ensure root element does not bounce excessively */
            hyperlink:"http://www.google.com",
            parent:null
        };
        return currState;
    }

    /***
     * Given a DOM element, parse the text to return only the relevant data
     * @param selectedDOMElement : Input DOM element
     * @return {* Text in DOM element}
     * TODO : Don't get the entire data.  Just filter in here for node value and relationship value etc
     */
    function _getSelectedText(selectedDOMElement) {
        var selectedText = selectedDOMElement.innerText || selectedDOMElement.textContent; //Firefox does not support innerText
        var nameText,
            c2pText,
            p2cText;
        if (selectedText !== undefined && selectedText != "") {
            
            nameText = _parseSelectedText(selectedText, mm.notes.defaults.parser.nodeTag, mm.notes.defaults.parser.nodeTag,
                                    mm.notes.defaults.parser.textStartIndex, mm.notes.defaults.parser.textEndIndex);

            c2pText = _parseSelectedText(selectedText, mm.notes.defaults.parser.c2PrelationTag,
                mm.notes.defaults.parser.c2PrelationTag,
                0, 0);
            p2cText = _parseSelectedText(selectedText, mm.notes.defaults.parser.p2CrelationTag,
                mm.notes.defaults.parser.p2CrelationTag,
                0, 0);

        }
        return {
            "text" : nameText,
            "c2p" : c2pText,
            "p2c" : p2cText
        }
    }


    function _parseSelectedText(selectedText, startTag, endTag, startIndex, length) {
        var endIndex = 0;
        if (selectedText.indexOf(startTag) >= 0) {
            startIndex = selectedText.indexOf(startTag) + startTag.length;
            var tempTxt =  selectedText.substring(startIndex);
            endIndex = tempTxt.indexOf(endTag);
            if (endIndex >= 0) {
                length = endIndex;
            }
        }

        if (length > 0){
            return selectedText.substring(startIndex, startIndex + length);

        }
        else return undefined;
    }

}();
