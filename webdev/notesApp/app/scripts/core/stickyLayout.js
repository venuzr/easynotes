/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 8/30/12
 * Time: 8:07 PM
 * To change this template use File | Settings | File Templates.
 * Based on http://css-tricks.com/snippets/css/sticky-footer/
 */

/* Ensure that the footer is positioned such that it is always visible at the bottom of the page
 * This is the Revealing module pattern as per
 * http://addyosmani.com/resources/essentialjsdesignpatterns/book/#revealingmodulepatternjavascript
 */


//http://elegantcode.com/2011/02/15/basic-javascript-part-10-the-module-pattern/
/********Need to figure out how to pass parameters *****/
mm.layout.positioning = function() {
    var shouldPush = false;
    /***********Private methods ************/
    /*
     *Ensure that the Editor panels(Editor and preview) are resized so that they take over the rest of
     * the div contents apart from header and footer
     */
    function _adjustEditorHeight() {
        
        var headerHeight = $('#navBarHeaderId').height(),
            footerHeight = $("#footer").height(),
            stickyFooterPush = $("#sticky-footer-push");
        var diffHeight = headerHeight + footerHeight;
        $('.editorial_settings').height($(window).height() - diffHeight);
    }


    /**
     * Ensure that the footer div is always at the bottom of the page.
     * Note sticky footer is different from absolute footer in that absolute footer would overlap other DOM elements
     * while sticky footer would not
     * @param shouldPush : boolean value indicating whether extra block should be added for the push
     */
    function _rePositionFooter(shouldPush) {
        var docHeight = $(document.body).height();

        if ($("#sticky-footer-push").height() !== undefined)
            docHeight = $(document.body).height() - $("#sticky-footer-push").height();
        if (docHeight < $(window).height()) {
            var diff = $(window).height() - docHeight;
            if (!$("#sticky-footer-push").length > 0) {
                $("#footer").before('<div id="sticky-footer-push"></div>');
            }

            if (shouldPush === true) ///When will this be necessary
            {
                /* Need to figure out what to do in situations where
                 the stickyFooter push might need to be implemented
                 */
                $("#sticky-footer-push").height(diff);
            }
        }
    }


    function rePositionLayouts(shouldPush) {
       //_rePositionFooter(shouldPush);
       _adjustEditorHeight();
    }


    // Reveal public pointers to
    // private functions and properties
    return {
            rePositionLayouts: rePositionLayouts
    };
}();


