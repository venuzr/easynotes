/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 10/15/12
 * Time: 11:40 AM
 * To change this template use File | Settings | File Templates.
 */

mm.core.StringUtils = function () {
    function searchForSubString(bigString, pattern, delimiter) {
        if (bigString !== undefined && bigString !== null) {
            var strArr = bigString.split(delimiter);
            for (var i = 0; i < strArr.length; i++) {
                if (strArr[i] === pattern) {
                    return true;
                }
            }
        }
        return false;
    }

    // Reveal public pointers to private functions and properties
    return {
        searchForSubString:searchForSubString
    };
}();



