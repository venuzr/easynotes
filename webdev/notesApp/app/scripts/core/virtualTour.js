/****************************************************************
 * Author : Venu
 * Date   : 2/7/13
 * Time   : 5:45 PM
 * Description : Helps provide a guided tour for the editor.
 *              Uses library https://github.com/jeff-optimizely/Guiders-JS
 *****************************************************************/

mm.core.virtualTour = function() {
    /***********Private methods ************/
    function registerTour() {

        guiders.createGuider({
            buttons: [{name: "Close", classString: "btn"},{name: "Next", classString: "btn btn-primary"}],
            id: "first",
            next: "editorStep",
            overlay: true,
            description: "<div class='tourSteps'>A simple visualization & note taking tool in your browser." +
                "<span><img style=\"width: 100%;\" style=\"height: 100%;\" src=\"/img/custom/Memnote_preview.png\"/></span>"  +
                "</div>",
            title: "<div class='tourTitle'>Welcome to Memnote.me</div>"
        }).show();


        guiders.createGuider({
            attachTo: "#editor1",
            buttons: [{name: "Close", classString: "btn"},{name: "Next", classString: "btn btn-primary"}],
            id: "editorStep",
            next: "editMode",
            position: 9,
            description: "<div class='tourSteps'>You can enter your notes in text/markdown/html in the editor</div>",
            title: "<div class='tourTitle'>Editor</div>",
            width: 200
        });

        guiders.createGuider({
            attachTo: "#editMode",
            buttons: [{name: "Close", classString: "btn"},{name: "Next", classString: "btn btn-primary"}],
            id: "editMode",
            next:"previewReading",
            position: 6,
            description: "<div class='tourSteps'>Click here for edit mode</div>",
            title: "<div class='tourTitle'>Edit Mode</div>",
            width: 200
        });

        guiders.createGuider({
            attachTo: "#previewMode",
            buttons: [{name: "Close", classString: "btn"},{name: "Next", classString: "btn btn-primary"}],
            id: "previewMode",
            next:"previewMM",
            position: 6,
            description: "<div class='tourSteps'>Click here for Preview mode</div>",
            title: "<div class='tourTitle'>Preview Mode</div>",
            width: 200
        });

        guiders.createGuider({
            attachTo: "#preview",
            buttons: [{name: "Close", classString: "btn"},{name: "Next", classString: "btn btn-primary"}],
            id: "previewReading",
            next:"previewMode",
            position: 3,
            description: "<div class='tourSteps'>Reading results for your notes</div>",
            title: "<div class='tourTitle'>Edit mode</div>",
            width: 200
        });

        guiders.createGuider({
            attachTo: "#previewWithMindMap",
            buttons: [{name: "Close", classString: "btn btn-primary"}],
            id: "previewMM",
            position: 3,
            description: "<div class='tourSteps'>Visualization of your notes</div>",
            title: "<div class='tourTitle'>Preview mode</div>",
            width: 200
        });
    }
 // Reveal public pointers to
    // private functions and properties
    return {
        registerTour: registerTour
    };
}();