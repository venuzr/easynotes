/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 10/17/12
 * Time: 2:06 PM
 * To change this template use File | Settings | File Templates.
 */

mm.core.visualizations.TreeNonInteractive = function (dataTree)  {

    function findMaxDepth(drawTree) {
        var depth = 1;
        if (drawTree.children !== null && drawTree.children !== undefined){
            for (var i=0;i<drawTree.children.length;i++){
                var ithChildDepth = 1 + findMaxDepth(drawTree.children[i]);
                if (ithChildDepth > depth)
                    depth = ithChildDepth;
            }
        }
        return depth;
    }

    var treeDepth = findMaxDepth(dataTree[0]);

    var margin = {top: 20, right:120, bottom: 20, left: 100},
        width = Math.round((1280 - margin.right - margin.left) * (treeDepth)) / 6 ,
        height = Math.round((1000 - margin.top - margin.bottom) * (treeDepth)) / 6,
        i = 0,
        duration = 400,
        root;

    var epsilon = 1.9;   // change for greater "hand-drawn" effect

    var tree = d3.layout.tree()
        .size([height, width])
        .separation(function(a, b) { return a.parent === b.parent ? 1 : 1; });

    var diagonal = d3.svg.diagonal()
        .projection(function(d) {
            var jitter = Math.random() * (2*epsilon) - epsilon;
            return [d.y, d.x];
        });

    d3.select("svg")
        .remove(); /** Clear all the contents for now.  Do a complete refresh of diagram when there is an edit
     TODO : Do only partial update after figuring out what the change is.
     Need to figure out a diff mechanism
     **/

    var vis = d3.select("#previewWithMindMap").append("svg")
            .attr("width", width + margin.right + margin.left)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            //.attr("transform",  "rotate(65) translate(" + 5 * margin.left + "," + margin.top/3 + ")");
            .attr("transform",  " translate(" + margin.left + "," + margin.top + ")");


    (function() {
        if (dataTree == undefined || dataTree == null || dataTree[0] === undefined || root === null)
            return;

        root = dataTree[0];
        root.x0 = height / 5;
        root.y0 = 0;


        update(root);
    })();

    function update(source) {
        var treeNodes = tree.nodes(root);
        var link = vis.selectAll("path.link")
                    .data(tree.links(treeNodes))
                 .enter().append("path")
                   .attr("class", "link")
                   .attr("d", diagonal)

        placeNodes();
        addLinkText();

        function placeNodes() {
            var drawNodes = getDrawNodes();
            drawTreeNodes();
            writeNodeText();

            function getDrawNodes() {
                var drawNodes = vis.selectAll("g.node")
                    .data(treeNodes)
                    .enter().append("g")
                    .attr("class", "drawNodes")
                    .attr("transform", function (d) {
                        return "translate(" + d.y + "," + d.x + ")";
                    })
                return drawNodes;
            }


            function drawTreeNodes() {
                var parentNodes = drawNodes.filter(function (d) {
                    return (d.children || d._children);
                });
                var childNodes = drawNodes.filter(function (d) {
                    return !(d.children || d._children);
                });
                parentNodes.append("rect")
                    .attr("width", function (d) {
                        return d.name.length * 8;
                    })
                    .attr("height", function (d) {
                        return 40;
                    })
                    .attr("x", function (d) {
                        return -d.name.length * 4;
                    })
                    .attr("y", function (d) {
                        return -20;
                    })
                    .attr("rx", function (d) {
                        return "10px";
                    })
                    .attr("ry", function (d) {
                        return "10px";
                    })
                    .attr("class", function (d) {
                        return "node heading" + d.level.toString() + "parent";
                    })
                childNodes.append("rect")
                    .attr("width", function (d) {
                        return  5;
                    })
                    .attr("height", function (d) {
                        return 5;
                    })
                    .attr("x", function (d) {
                        return  0;
                    })
                    .attr("y", function (d) {
                        return  0;
                    })
                    .attr("rx", function (d) {
                        return  0;
                    })
                    .attr("ry", function (d) {
                        return  0;
                    })
                    .attr("class", function (d) {
                        return "node heading" + d.level.toString() + "child";
                    })
                /*   node.append("circle")
                 .attr("r", function (d) {
                 return (d.children || d._children) ? 40 : 5;
                 })
                 .attr("class", function (d) {
                 return "node heading" + d.level.toString() + ((d.children || d._children) ? "parent" : "child");
                 })*/
            }

            function writeNodeText() {
                drawNodes.append("text")
                    .attr("class", "nodeText")
                    .attr("dy", ".35em")
                    .attr("x", function (d) {
                        return d.children || d._children ? 0 : 10; //Relative positioning depending on having children
                    })
                    .attr("text-anchor", function (d) {
                        return d.children ? "middle" : "start";
                    })
                    .text(function (d) {
                        return d.name;
                    });
            }
        }


       function addLinkText() {
           /*****Add link text *****/
           $('.linkText').remove();
           var linkText = vis.selectAll("g.link")
               .data(function(d) {
                   var nodeLinks = tree.links(treeNodes);
                   return nodeLinks.filter(function(nl){
                       return nl.target.c2PrelationShip !== undefined
                   });
               });

           var linkTextEnter = linkText
               .enter().append("g")
               .attr("class", "linkText");

           linkTextEnter.append("text")
               .text(function (d) {
                   return d.target.c2PrelationShip;
               })
               .attr("transform",  function(d) {
                   ///http://stackoverflow.com/questions/1707151/finding-angles-0-360
                   if (d.target.c2PrelationShip !== undefined) {

                       function calculateAngle(d) {
                           var deltaX = d.target.x - d.source.x;
                           var deltaY = d.target.y - d.source.y;
                           return Math.atan(deltaX / deltaY) * 180 / (Math.PI );
                       }
                       var angle = calculateAngle(d);
                       var dy = ((d.source.y + d.target.y) / 2);
                       var dx = (d.source.x + d.target.x) / 2;

                       return "rotate(" + angle + ","  + dy + "," + dx + ") translate(" + dy + "," + dx + ")";
                   }
                   return "";
               })
               .attr("class", "pathText");
       }


    }
}
