mm.core.visualizations.ClusterRadialMindMap = function (dataTree, displayDepth) {
    var radius = 960 / 2;
    var xBarrier = 180;


    d3.select("svg")
        .remove(); /** Clear all the contents for now.  Do a complete refresh of diagram when there is an edit
     TODO : Do only partial update after figuring out what the change is.
     Need to figure out a diff mechanism
     **/

    var cluster = d3.layout.tree()
        .size([360, radius]);

    var diagonal = d3.svg.diagonal.radial()
        .projection(function (d) {
            //return [d.y, d.x / 180 * Math.PI];
            return [d.y, d.x / 180 * Math.PI];
        });

    function elbow(d, i) {
        return "M" + d.source.y + "," + d.source.x
            + "V" + d.target.x + "H" + d.target.y;
    }

    var vis = d3.select("#chart").append("svg")
        .attr("width", radius * 3)
        .attr("height", radius * 3)
        .append("g")
        .attr("transform", "translate(" + radius * 1.5 + "," + radius * 1.5 + ")")
        ;

    (function() {
        console.log(dataTree);

        var treeNodes = cluster.nodes(dataTree[0]);
        var link = vis.selectAll("path.link")
            .data(cluster.links(treeNodes))
            .enter().append("path")
            .attr("class", "link")
            .attr("d", diagonal);

        drawClusterNodes();
        writeLinkText();


        function writeLinkText() {
            /*****Add link text *****/
            $('.linkText').remove();
            var linkText = vis.selectAll("g.link")
                .data(function (d) {
                    var nodeLinks = cluster.links(treeNodes);
                    return nodeLinks.filter(function (nl) {
                        return nl.target.c2PrelationShip !== undefined
                    });
                });
            var linkTextEnter = linkText
                .enter()
                .append("g")
                .attr("class", "linkText");
            linkTextEnter
                .append("text")
                .text(function (d) {
                    return d.target.c2PrelationShip;
                })
                .attr("transform", function (d) {
                    ///http://stackoverflow.com/questions/1707151/finding-angles-0-360
                    if (d.target.c2PrelationShip !== undefined) {
                        console.log("target is " + d.target.x + ", " + d.target.y);
                        console.log("source is " + d.source.x + ", " + d.source.y);
                        function calculateAngle(d) {
                            var deltaX = d.target.x - d.source.x;
                            var deltaY = d.target.y - d.source.y;
                            var angle = Math.atan(deltaX / deltaY) * 180 / (Math.PI );
                            function adjustAngle() {
                                var add = 0;
                                if (deltaX < 0) {
                                    add = 180;
                                }
                                else if (deltaY < 0) {
                                    add = 360;
                                }
                                return add;
                            }
                            return angle;
                        }
                        var angle = calculateAngle(d);
                        var dy = ((d.source.y + d.target.y) / 2);
                        var dx = (d.source.x + d.target.x) / 2;
                        var transformStr = "rotate(" + angle + "," + dy + "," + dx + ")";
//                    var transformStr =  "rotate(" + angle + ","  + dy + "," + dx + ") translate(" + dy + "," + dx + ")";
                        //return dx < xBarrier ? "translate(8)" : "rotate(180)translate(-8)";
                        //console.log(transformStr);
                        return transformStr;
                    }
                    return "";
                })
                //.attr("text-anchor", "middle")
                .attr("class", "pathText");
            /*
             linkTextEnter.append("text")
             .attr("x", function (d) {
             return (d.source.x + d.target.x) / 2;
             })
             .attr("y", function (d) {
             return (d.source.y + d.target.y) / 2;
             })
             .attr("text-anchor", "middle")
             .text(function (d) {
             return d.target.name + d.target.c2PrelationShip;
             })*/
        }


        function drawClusterNodes() {
            var node = vis.selectAll("g.node")
                .data(treeNodes)
                .enter().append("g")
                .attr("transform", function (d) {
                    return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")";
                })

            var childNodes = node.filter(function (d) {
                return !(d.children || d._children);
            });


            var parentNodes = node.filter(function (d) {
                return (d.children || d._children);
            });
            parentNodes.append("rect")
                .attr("width", function (d) {
                    return d.name.length * 8;
                })
                .attr("height", function (d) {
                    return 40;
                })
                .attr("x", function (d) {
                    return -d.name.length * 4;
                })
                .attr("y", function (d) {
                    return -20;
                })
                .attr("rx", function (d) {
                    return "10px";
                })
                .attr("ry", function (d) {
                    return "10px";
                })
                //.attr("r", 35)
                .attr("class", function (d) {
                    return "node heading" + d.level.toString() + "parent";
                })
                .attr("transform", function (d) {
                    return parentTransform(d);
                })
            parentNodes.append("text")
                .attr("text-anchor", function (d) {
                    return "middle";
                })
                .attr("transform", function (d) {
                    return  parentTransform(d);
                })
                .text(function (d) {
                    console.log("Name is " + d.name);
                    console.log("Coord are " + d.x + " " + d.y);
                    console.log("Tranformation is " + parentTransform(d));

                    return d.name;
                })
/*
            function  parentTransform(d) {
                if (d.y === 0 || d.y === xBarrier * 2) {
                    if (d.x < xBarrier / 2 || d.x > xBarrier * 3 / 2)
                        return "rotate(90)translate(8)";
                    return "rotate(270)translate(8)";
                }
                else if (d.x < xBarrier && d.y < xBarrier)
                    return "rotate(-90)translate(8)";
                else if (d.x < xBarrier && d.y > xBarrier)
                    return "rotate(90)translate(8)";
                else if (d.x > xBarrier && d.y > xBarrier)
                    return "rotate(270)translate(8)";
                else return "translate(8)";
            }*/

             function  parentTransform(d) {
                            if (d.y === 0 || d.y === xBarrier * 2) {
                                if (d.x < xBarrier / 2 || d.x > xBarrier * 3 / 2)
                                    return "rotate(90)translate(8)";
                                return "rotate(270)translate(8)";
                            }
                            else if (d.x < xBarrier && d.y < xBarrier)
                                return "rotate(-90)translate(8)";
                            else if (d.x < xBarrier && d.y > xBarrier)
                                return "rotate(90)translate(8)";
                            else if (d.x > xBarrier && d.y > xBarrier)
                                return "rotate(270)translate(8)";
                            else return "translate(8)";
                        }

            childNodes.append("circle")
                .attr("r", 3)
                .attr("class", function (d) {
                    return "node heading" + d.level.toString() + "child";
                })
            childNodes.append("text")
                .attr("dy", ".31em")
                .attr("text-anchor", function (d) {
                    return d.x < xBarrier ? "start" : "end";
                })
                .attr("transform", function (d) {
                    return d.x < xBarrier ? "translate(8)" : "rotate(180)translate(-8)";
                })
                .text(function (d) {
                    return d.name;
                });
        }

    })();
}
