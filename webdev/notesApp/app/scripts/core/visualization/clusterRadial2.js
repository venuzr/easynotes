/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 9/25/12
 * Time: 2:45 PM
 * To change this template use File | Settings | File Templates.
 */

mm.core.visualizations.ClusterRadial2 = function (dataTree) {
    var r = 400;

    var margin = {top: 20, right:120, bottom: 20, left: 50},
        width = 1280 - margin.right - margin.left,
        height = 700 - margin.top - margin.bottom,
        i = 0,
        duration = 400,
        root;

    d3.select("svg")
        .remove(); /** Clear all the contents for now.  Do a complete refresh of diagram when there is an edit
     TODO : Do only partial update after figuring out what the change is.
     Need to figure out a diff mechanism
     **/

    var svg = d3.select("#chart").append("svg")
        .attr("width", r*4)
        .attr("height", r*3.2)
        .style("background","#fff")
        .append("g")
        .attr("transform", "translate(" + r*1 + "," + (r*1) + ")")

    /*Need to remove below statement.  Currently removing this causing the svg declaration above to blow up*/
    svg.append("text")
        .text("WEST").attr("class","region")
        .attr("x", -r*1.5)
        .attr("y", r*1)
        .attr("text-anchor","middle");


    (function() {
        console.log(dataTree);

        root = dataTree[0];
        root.x0 = height / 5;
        root.y0 = 0;

        // time to visualize!
        var tree = d3.layout.tree()
            .size([r * 1  , r ])
            .separation(function(a, b) {
                if (a.region != b.region) {
                    return 1;
                } else {
                    return (a.parent == b.parent ? 2 : 1) / a.depth;
                }
            });


        /*
        var diagonal = d3.svg.diagonal()
            .projection(function(d) {
                return [d.y, d.x];
            });
            */

        var diagonal = d3.svg.diagonal.radial()
            .projection(function(d) {
                return [d.y ,   d.x / 180 * Math.PI];

            });

        var nodes = tree.nodes(dataTree[0]);
        var links = tree.links(nodes);

        var drawlink = svg.selectAll("path.link")
            .data(links)
            .enter()
            .append("path")
            .attr("class", "link")
            .attr("d", diagonal);

        var drawNode = svg.selectAll("g.node")
            .data(nodes)
            .enter().append("g")
            .attr("class","node")
            .attr("transform", function(d) { return "rotate(" + (d.x-90) + ")translate(" + d.y + ")"; })


        var parentNodes = drawNode.filter(function (d) {
            return (d.children || d._children);
        });

        var childNodes = drawNode.filter(function (d) {
            return !(d.children || d._children);
        });
        parentNodes
            .append("circle").attr("r",35)
            .style("stroke", "#ccc").style("stroke-width", "2px")
            .style("fill", "yellow");


        /********Write the node text ****/
        parentNodes
            .append("text")
            .attr("class","nodeBox")
            .attr("text-anchor", function(d) { return "middle" })
            //.attr("transform", function(d) { return d.x < 180 ? null : "rotate(180)"; })
            .text(function(d) { return d.name; });


        childNodes
            .append("circle").attr("r",4)
            .style("stroke", "#ccc").style("stroke-width", "2px")
            .style("fill", "lightsteelblue");
        childNodes
            .append("text")
            .attr("class","nodeBox")
            .attr("x", function(d) { return d.x < 180 ? 40 : -40; })
            .attr("y", 10)
            .attr("text-anchor", function(d) { return d.x < 180 ? "start" : "end"; })
            .attr("text-anchor", function(d) { return "middle" })
            .attr("transform", function(d) { return d.x < 180 ? null : "rotate(180)"; })
            .text(function(d) { return d.name; });

        /*****Draw the relationships ***/
        /*
        drawNode
            .filter(function(d){ return d.level > 1;})//THere is no parent for the root element
            .append("text")
            .attr("x", function(d) { return d.x < 180 ? 25 : -25; })
            .attr("y", 10)
            .attr("text-anchor", "middle")
            .attr("transform", function(d) { return d.x < 180 ? null : "rotate(180)"; })
            .attr("class","p2cRelationShip")
            .text(function(d) { return d.name; });
          */

    })();   /********Declare the function and invoke it immediately  ***/



};

