/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 9/26/12
 * Time: 11:40 AM
 * To change this template use File | Settings | File Templates.
 */

mm.core.visualizations.ElbowMap = function (dataTree) {
    var margin = {top:0, right:320, bottom:0, left:0},
        width = 960 - margin.left - margin.right,
        height = 800 - margin.top - margin.bottom;

    var tree = d3.layout.tree()
        .separation(function (a, b) {
            return a.parent === b.parent ? .5 : 0.4;
        })
        .children(function (d) {
            return d.children;
        })
        .size([height, width]);

    var svg = d3.select("#chart").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    (function () {
        var nodes = tree.nodes(dataTree[0]);

        var link = svg.selectAll(".link")
            .data(tree.links(nodes))
            .enter().append("path")
            .attr("class", "link")
            .attr("d", elbow);

        var node = svg.selectAll(".node")
            .data(nodes)
            .enter().append("g")
            .attr("class", "node")
            .attr("transform", function (d) {
                return "translate(" + d.y + "," + d.x + ")";
            })

        node.append("text")
            .attr("class", "name")
            .attr("x", 8)
            .attr("y", -6)
            .text(function (d) {
                return d.name;
            });

        /*
         node.append("text")
         .filter(function(d){ return d.level > 1;})
         .filter(function(d){ return d.children;})
         .attr("x", 8)
         .attr("y", 8)
         .attr("dy", ".71em")
         .attr("dx", "5em")
         .attr("class", "about lifespan")
         .text(function(d) { return d.c2PrelationShip; });
         */
        /*
         node.append("text")
         .attr("x", 8)
         .attr("y", 8)
         .attr("dy", "1.86em")
         .attr("class", "about location")
         .text(function(d) { return d.c2PrelationShip; });*/
    })();

    function elbow(d, i) {
        return "M" + d.source.y + "," + d.source.x
            + "H" + d.target.y + "V" + d.target.x
            + (d.target.children ? "" : "h" + margin.right);
    }


}