/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 10/20/12
 * Time: 2:48 PM
 * To change this template use File | Settings | File Templates.
 */

//http://enjalot.com/tributary/3923929/

mm.core.visualizations.forceDirectedRadial = function (dataTree) {
    var focalNodeID = "N1";

    var nodeSet = [
        {id: "N1", name: "Node 1", type: "Type 1", hlink: "http://www.if4it.com"},
        {id: "N2", name: "Node 2", type: "Type 3", hlink: "http://www.if4it.com/glossary.html"},
        {id: "N3", name: "Node 3", type: "Type 4", hlink: "http://www.if4it.com/resources.html"},
        {id: "N4", name: "Node 4", type: "Type 5", hlink: "http://www.if4it.com/taxonomy.html"},
        {id: "N5", name: "Node 5", type: "Type 1", hlink: "http://www.if4it.com/disciplines.html"},
        {id: "N6", name: "Node 6", type: "Type 2", hlink: "http://www.if4it.com"},
        {id: "N7", name: "Node 7", type: "Type 3", hlink: "http://www.if4it.com/glossary.html"},
        {id: "N8", name: "Node 8", type: "Type 4", hlink: "http://www.if4it.com/resources.html"},
        {id: "N9", name: "Node 9", type: "Type 5", hlink: "http://www.if4it.com/taxonomy.html"},
        {id: "N10", name: "Node 10", type: "Type 1", hlink: "http://www.if4it.com/disciplines.html"},
        {id: "N11", name: "Node 11", type: "Type 2", hlink: "http://www.if4it.com"},
        {id: "N12", name: "Node 12", type: "Type 3", hlink: "http://www.if4it.com/glossary.html"},
        {id: "N13", name: "Node 13", type: "Type 4", hlink: "http://www.if4it.com/resources.html"},
        {id: "N14", name: "Node 14", type: "Type 5", hlink: "http://www.if4it.com/taxonomy.html"},
        {id: "N15", name: "Node 15", type: "Type 1", hlink: "http://www.if4it.com/disciplines.html"},
        {id: "N16", name: "Node 16", type: "Type 3", hlink: "http://www.if4it.com"},
        {id: "N17", name: "Node 17", type: "Type 1", hlink: "http://www.if4it.com/glossary.html"},
        {id: "N18", name: "Node 18", type: "Type 1", hlink: "http://www.if4it.com/glossary.html"},
        {id: "N19", name: "Node 19", type: "Type 4", hlink: "http://www.if4it.com/glossary.html"},
        {id: "N20", name: "Node 20", type: "Type 4", hlink: "http://www.if4it.com/glossary.html"},
        {id: "N21", name: "Node 21", type: "Type 4", hlink: "http://www.if4it.com/glossary.html"},
        {id: "N22", name: "Node 22", type: "Type 1", hlink: "http://www.if4it.com/glossary.html"},
        {id: "N23", name: "Node 23", type: "Type 5", hlink: "http://www.if4it.com/glossary.html"},
        {id: "N24", name: "Node 24", type: "Type 5", hlink: "http://www.if4it.com/glossary.html"},
        {id: "N25", name: "Node 25", type: "Type 5", hlink: "http://www.if4it.com/glossary.html"},
        {id: "N26", name: "Node 26", type: "Type 5", hlink: "http://www.if4it.com/glossary.html"},
        {id: "N27", name: "Node 27", type: "Type 5", hlink: "http://www.if4it.com/glossary.html"},
        {id: "N28", name: "Node 28", type: "Type 5", hlink: "http://www.if4it.com/glossary.html"},
        {id: "N29", name: "Node 29", type: "Type 5", hlink: "http://www.if4it.com/glossary.html"}

    ];

    var linkSet = [
        {sourceId: "N1", linkName: "Relationship 1", targetId: "N2"},
        {sourceId: "N3", linkName: "Relationship 2", targetId: "N1"},
        {sourceId: "N4", linkName: "Relationship 3", targetId: "N1"},
        {sourceId: "N1", linkName: "Relationship 4", targetId: "N5"},
        {sourceId: "N6", linkName: "Relationship 5", targetId: "N1"},
        {sourceId: "N1", linkName: "Relationship 6", targetId: "N7"},
        {sourceId: "N1", linkName: "Relationship 7", targetId: "N8"},
        {sourceId: "N9", linkName: "Relationship 8", targetId: "N1"},
        {sourceId: "N1", linkName: "Relationship 9", targetId: "N10"},
        {sourceId: "N11", linkName: "Relationship 10", targetId: "N1"},
        {sourceId: "N1", linkName: "Relationship 11", targetId: "N12"},
        {sourceId: "N13", linkName: "Relationship 12", targetId: "N1"},
        {sourceId: "N1", linkName: "Relationship 13", targetId: "N14"},
        {sourceId: "N1", linkName: "Relationship 14", targetId: "N15"},
        {sourceId: "N16", linkName: "Relationship 15", targetId: "N1"},
        {sourceId: "N17", linkName: "Relationship 16", targetId: "N1"},
        {sourceId: "N18", linkName: "Relationship 17", targetId: "N1"},
        {sourceId: "N19", linkName: "Relationship 18", targetId: "N1"},
        {sourceId: "N20", linkName: "Relationship 19", targetId: "N1"},
        {sourceId: "N21", linkName: "Relationship 20", targetId: "N1"},
        {sourceId: "N22", linkName: "Relationship 21", targetId: "N1"},
        {sourceId: "N1", linkName: "Relationship 22", targetId: "N23"},
        {sourceId: "N1", linkName: "Relationship 23", targetId: "N24"},
        {sourceId: "N24", linkName: "Relationship 24", targetId: "N25"},
        {sourceId: "N24", linkName: "Relationship 25", targetId: "N26"},
        {sourceId: "N24", linkName: "Relationship 26", targetId: "N27"},
        {sourceId: "N24", linkName: "Relationship 27", targetId: "N28"},
        {sourceId: "N24", linkName: "Relationship 28", targetId: "N29"},

    ];


    //console.log(linkSet)


    var width = 1044,
        height = 922,
        centerNodeSize = 50;
    nodeSize = 10;
    colorScale = d3.scale.category20();

    var svgCanvas = d3.select("svg")
        .attr("width", width)
        .attr("height", height);

    var node_hash = [];
    var type_hash = [];


    nodeSet.forEach(function(d, i) {
        node_hash[d.id] = d;
        type_hash[d.type] = d.type;
    });
    // Append the source object node and the target object node to each link
    linkSet.forEach(function(d, i) {
        d.source = node_hash[d.sourceId];
        d.target = node_hash[d.targetId];
        if (d.sourceId == focalNodeID)
        { d.direction = "OUT"; }
        else
        { d.direction = "IN"; }
    });

    // Create a force layout and bind Nodes and Links
    var force = d3.layout.force()
        .charge(-4673)
        .nodes(nodeSet)
        .links(linkSet)
        .size([width, height])
        .linkDistance( function(d) { if (width < height) { return width*1/3; } else { return height*1/3 } } ) // Controls edge length
        .on("tick", tick)
        .start();


    // Draw lines for Links between Nodes
    var link = svgCanvas.selectAll(".gLink")
        .data(force.links())
        .enter().append("g")
        .attr("class", "gLink")
        .append("line")
        .attr("class", "link")
        .style("stroke", "#ccc")
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    // Create Nodes
    var node = svgCanvas.selectAll(".node")
        .data(force.nodes())
        .enter().append("g")
        .attr("class", "node")
        .on("mouseover", nodeMouseover)
        .on("mouseout", nodeMouseout)
        .call(force.drag);

    // Append circles to Nodes
    node.append("circle")
        .attr("x", function(d) { return d.x; })
        .attr("y", function(d) { return d.y; })
        .attr("r", function(d) { if (d.id==focalNodeID) { return centerNodeSize; } else { return nodeSize; } } ) // Node radius
        .style("fill", "White") // Make the nodes hollow looking
        .style("stroke-width", 5) // Give the node strokes some thickness
        .style("stroke", function(d, i) { colorVal = colorScale(i); return colorVal; } ) // Node stroke colors
        .call(force.drag);

    // Append text to Nodes
    node.append("a")
        .attr("xlink:href", function(d) { return d.hlink; })
        .append("text")
        .attr("x", function(d) { if (d.id==focalNodeID) { return 0; } else {return 20;} } )
        .attr("y", function(d) { if (d.id==focalNodeID) { return 0; } else {return -10;} } )
        .attr("text-anchor", function(d) { if (d.id==focalNodeID) {return "middle";} else {return "start";} })
        .attr("font-family", "Arial, Helvetica, sans-serif")
        .style("font", "normal 16px Arial")
        .attr("fill", "Blue")
        .attr("dy", ".35em")
        .text(function(d) { return d.name; });

    // Append text to Link edges
    var linkText = svgCanvas.selectAll(".gLink")
        .data(force.links())
        .append("text")
        .attr("font-family", "Arial, Helvetica, sans-serif")
        .attr("x", function(d) {
            if (d.target.x > d.source.x) { return (d.source.x + (d.target.x - d.source.x)/2); }
            else { return (d.target.x + (d.source.x - d.target.x)/2); }
        })
        .attr("y", function(d) {
            if (d.target.y > d.source.y) { return (d.source.y + (d.target.y - d.source.y)/2); }
            else { return (d.target.y + (d.source.y - d.target.y)/2); }
        })
        .attr("fill", "Maroon")
        .style("font", "normal 12px Arial")
        .attr("dy", ".35em")
        .text(function(d) { return d.linkName; });

    function tick() {
        link
            .attr("x1", function(d) { return d.source.x; })
            .attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; })
            .attr("y2", function(d) { return d.target.y; });

        node
            .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

        linkText
            .attr("x", function(d) {
                if (d.target.x > d.source.x) { return (d.source.x + (d.target.x - d.source.x)/2); }
                else { return (d.target.x + (d.source.x - d.target.x)/2); }
            })
            .attr("y", function(d) {
                if (d.target.y > d.source.y) { return (d.source.y + (d.target.y - d.source.y)/2); }
                else { return (d.target.y + (d.source.y - d.target.y)/2); }
            });
    }

    function nodeMouseover() {
        d3.select(this).select("circle").transition()
            .duration(250)
            .attr("r", function(d,i) { if(d.id==focalNodeID) {return 65;} else {return 15;} } );
        d3.select(this).select("text").transition()
            .duration(250)
            .style("font", "bold 20px Arial")
            .attr("fill", "Blue");
    }

    function nodeMouseout() {
        d3.select(this).select("circle").transition()
            .duration(250)
            .attr("r", function(d,i) { if(d.id==focalNodeID) {return centerNodeSize;} else {return nodeSize;} } );
        d3.select(this).select("text").transition()
            .duration(250)
            .style("font", "normal 16px Arial")
            .attr("fill", "Blue");
    }



}







