/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 9/22/12
 * Time: 11:44 AM
 * To change this template use File | Settings | File Templates.
 */

mm.core.visualizations.TreeInteractive = function (dataTree)  {

    var margin = {top: 20, right:120, bottom: 20, left: 100},
        width = 1280 - margin.right - margin.left,
        height = 700 - margin.top - margin.bottom,
        i = 0,
        duration = 400,
        root;

    var tree = d3.layout.tree()
        .size([height, width])
        .separation(function(a, b) { return a.parent === b.parent ? 1 : 1; });

    var diagonal = d3.svg.diagonal()
        .projection(function(d) {
            return [d.y, d.x];
        });

    d3.select("svg")
        .remove(); /** Clear all the contents for now.  Do a complete refresh of diagram when there is an edit
                        TODO : Do only partial update after figuring out what the change is.
                        Need to figure out a diff mechanism
                    **/



    var vis = d3.select("#previewWithMindMap").append("svg")
        .attr("width", width + margin.right + margin.left)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    (function() {
        if (dataTree == undefined || dataTree == null || dataTree[0] === undefined || root === null)
            return;

        root = dataTree[0];
        root.x0 = height / 5;
        root.y0 = 0;

        function collapse(d) {
            if (d.children) {
                d._children = d.children;
                d._children.forEach(collapse);
                d.children = null;
            }
        }

        if (root.children !== undefined && root.children !== null)
            root.children.forEach(collapse);
        update(root);
    })();   /********Declare the function and invoke it immediately  ***/

    function update(source) {

        // Compute the new tree layout.
        var nodes = tree.nodes(root).reverse();

        // Normalize for fixed-depth.
        nodes.forEach(function(d) { d.y = d.depth * 180; });

        // Update the nodes…    Is this the key function?
        var node = vis.selectAll("g.node")
            .data(nodes, function(d) {
                return d.id || (d.id = ++i);
            });

        /*****Add link text *****/
        $('.linkText').remove();
        var linkText = vis.selectAll("g.link")
            .data(tree.links(nodes));
        var linkTextEnter = linkText
            .enter().append("g")
            .attr("class", "linkText");

        // Enter any new nodes at the parent's previous position.
        var nodeEnter = node.enter().append("g")
            .attr("class", "node")
            .attr("transform", function(d) {
                return "translate(" + source.y0 + "," + source.x0 + ")";
            })
            .on("dblclick", dblClickHandler);

        nodeEnter.append("circle")
            .attr("r", 15)
            .attr("class", function (d) {
                return "node heading" + d.level.toString() + (d.children || d._children ? "parent" : "child");
            });

        nodeEnter
            .append("svg:a").attr("xlink:href", function(d) { return d.hyperlink; }) /*Add hyperlink*/
            .append("text")
            .attr("class", "nodeText")
            .attr("dy", ".35em")
            .attr("x", function(d) {
                return d.children || d._children ? 0 : 10; //Relative positioning depending on having children
            })

            .attr("text-anchor", function(d) {
                return d.children || d._children ? "middle" : "start"; //Relative positioning depending on having children
            })

            .text(function(d) { return d.name; })



        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
            .duration(duration)
            .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

        nodeUpdate.select("circle")
            .attr("r", function (d) {
                return (d.children || d._children) ? 40 : 5;
            })
            .attr("class", function (d) {
                return "node heading" + d.level.toString() + ((d.children || d._children) ? "parent" : "child");
            })

        nodeUpdate.select("text")


        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
            .duration(duration)
            .attr("transform", function(d) {
                return "translate(" + source.y + "," + source.x + ")";
            })
            .remove();

        nodeExit.select("circle")
            .attr("r", 1e-6);

        nodeExit.select("text")
            .style("fill-opacity", 1e-6);

        // Update the links…
        var link = vis.selectAll("path.link")
            .data(tree.links(nodes), function(d) { return d.target.id; });

        // Enter any new links at the parent's previous position.
        link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", function(d) {
                var o = {x: source.x0 , y: source.y0};
                return diagonal({source: o, target: o});
            });

        // Transition links to their new position.
        link.transition()
            .duration(duration)
            .attr("d", diagonal);

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
            .duration(duration)
            .attr("d", function(d) {
                var o = {x: source.x, y: source.y};
                return diagonal({source: o, target: o});
            })
            .remove();




        linkTextEnter.append("text")
            .attr("x", function (d) {
                return (d.source.y + d.target.y) / 2;
            })
            .attr("y", function (d) {
                return (d.source.x + d.target.x) / 2;
            })
            .attr("text-anchor", "middle")
            .text(function (d) {
                return d.target.name + d.target.c2PrelationShip;
            });

        // Stash the old positions for transition.
        nodes.forEach(function(d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });
    }

// Toggle children on click.
    function dblClickHandler(d) {
        if (d.children) {
            d._children = d.children;
            d.children = null;
        } else {
            d.children = d._children;
            d._children = null;
        }
        update(d);
    }

}

