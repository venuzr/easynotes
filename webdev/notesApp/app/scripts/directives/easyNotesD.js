/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 10/6/12
 * Time: 10:41 AM
 * To change this template use File | Settings | File Templates.
 */


'use strict';
/***
 * Priority is used for ordering of directives.  Higher number means it will fire first.
 *  101 is special and useful if string interpolation is being performed
 ***/

var maxPriority = 50;
angular.module('EasyNotesComponents', [])
    .directive("easynotesFooter", ['$parse', function ($parse) {
        return {
            restrict:'E',
            replace: true,
            priority:maxPriority - 2,
            templateUrl:'views/partials/footer.html',
            link:function (scope, element, attrs) {
                mm.layout.positioning.rePositionLayouts()
                $(window)
                    .scroll(mm.layout.positioning.rePositionLayouts)
                    .resize(function(){
                        $.doTimeout('resize', mm.notes.defaults.debounce.resize, function(){
                             /*This line needs to be here to ensure that the value used is the current one
                             * and not one used while creating the closure
                             */
                            mm.layout.positioning.rePositionLayouts();
                        });
                    });
            }
        }
    }])
    .directive("markdownEditor", ['$compile', function ($compile) {
        return {
            restrict:'A',
            priority:maxPriority,
            scope:{ },
            templateUrl:'views/partials/markdownEditor.html',
            //template:'<pre id="editor1" class=""></pre>',
            link:function (scope, element, attrs) {
                mm.layout.positioning.rePositionLayouts(true);
                scope.$parent.setViewingModeAndEditor();
            }
        }
    }])
    .directive("navbarHeader", ['$parse', function ($parse) {
    return {
        restrict:'E',
        priority:maxPriority - 1,
        replace: true,
        templateUrl:'views/partials/navbar-header.html',
        link:function (scope, element, attrs) {
            $('#fileNew').on('click', function (e) {
                mm.core.EditorEvents.setEditorContents('', mm.core.cookieHelper.get(mm.cookieKeys.viewMode));
                mm.core.cookieHelper.set(mm.cookieKeys.isNewFile, true,
                    mm.notes.defaults.cache.expiryMilliSeconds);
            });

            $('#fileSave').on('click', function (e) {
                function saveFile(fileNameToSave, isNewFile, fileContents, mimeType){
                    var fileArray = [];
                    fileArray.push(fileContents);
                    var blob = new Blob(fileArray, {type: "text/plain;charset=utf-8"});
                    saveAs(blob, fileNameToSave);
                }
                saveFile(mm.core.cookieHelper.get(mm.cookieKeys.currentOpenFile) || "temp.md",
                    mm.core.cookieHelper.get(mm.cookieKeys.isNewFile) === true,
                    mm.core.EditorEvents.getEditor().getSession().getValue(),
                    "application/octet-stream");

            });
            $('#startTour').on('click', function (e) {
                mm.core.virtualTour.registerTour();
            });


            /****Set current dropdown theme**/
            function setEditorThemeInDropdown() {
                var cookieTheme = mm.core.cookieHelper.get(mm.cookieKeys.theme);
                if (cookieTheme !== undefined &&
                    cookieTheme != mm.notes.defaults.editorTheme) {
                    
                    $('.editorThemeDropDown li').each(function(index) {
                        var anchElement = $(this).find('a')[0];
                        if (anchElement && anchElement.getAttribute('theme') === cookieTheme) {
                            $(this).addClass('active');
                        }
                        else {
                            $(this).removeClass('active');
                        }
                    });

                    
                }
            }


            $('.editorThemeDropDown li a').on('click', function (e) {
                e.preventDefault();
                var themeValue = e.target.getAttribute('theme');
                mm.core.EditorEvents.editorThemeSelectorChange(themeValue);
                setHeaderButtonStates();
                setEditorThemeInDropdown();
            });


            (function() {
                setHeaderButtonStates(); ///Ensure the header button states are set first time
                setEditorThemeInDropdown();
            })()
            
            /******* Toggle states in nav header ***/
            function setHeaderButtonStates() {
                switch (mm.core.cookieHelper.get(mm.cookieKeys.viewMode)) {

                    case mm.notes.displayMode.editing :
                        $('#editMode').addClass('active');
                        $('#readMode').removeClass('active');
                        $('#previewMode').removeClass('active');
                        break;
                    case mm.notes.displayMode.preview :
                        $('#editMode').removeClass('active');
                        $('#readMode').removeClass('active');
                        $('#previewMode').addClass('active');
                        break;
                    case mm.notes.displayMode.readingWithMindMap : /**Currently not implemented */
                        $('#editMode').removeClass('active');
                        $('#readMode').addClass('active');
                        $('#previewMode').removeClass('active');
                        break;
                    default:
                        mm.core.cookieHelper.set(mm.cookieKeys.viewMode, mm.notes.defaults.displayMode,
                            mm.notes.defaults.cache.expiryMilliSeconds)
                        $('#editMode').addClass('active');
                        $('#readMode').removeClass('active');
                        $('#previewMode').removeClass('active');
                        break;

                }
            }


            /****Set current mode ***/
            (function() {
                var viewingMode = null;
                $('#editMode').on('click', function (e) {
                    mm.core.cookieHelper.set(mm.cookieKeys.viewMode, mm.notes.displayMode.editing,
                        mm.notes.defaults.cache.expiryMilliSeconds);
                    setViewingMode();
                });
                $('#previewMode').on('click', function (e) {
                    mm.core.cookieHelper.set(mm.cookieKeys.viewMode, mm.notes.displayMode.preview,
                        mm.notes.defaults.cache.expiryMilliSeconds);
                    setViewingMode();
                });
                $('#readMode').on('click', function (e) {
                    mm.core.cookieHelper.set(mm.cookieKeys.viewMode, mm.notes.displayMode.readingWithMindMap,
                        mm.notes.defaults.cache.expiryMilliSeconds);
                    setViewingMode();
                });
                if(mm.core.cookieHelper.get(mm.cookieKeys.viewMode) === undefined){
                    mm.core.cookieHelper.set(mm.cookieKeys.viewMode, mm.notes.defaults.displayMode,
                        mm.notes.defaults.cache.expiryMilliSeconds);
                    setViewingMode();
                }



                function setViewingMode() {
                    setHeaderButtonStates();
                    mm.core.EditorEvents.setViewingMode(
                        mm.core.cookieHelper.get(mm.cookieKeys.viewMode), true);
                }


            })();   /********Declare the function and invoke it immediately  ***/
        }

    }
}])
    .directive("fileopenDialog", ['$parse', function ($parse) {
    return {
        restrict:'E',
        priority:maxPriority - 3,
        replace: true,
        templateUrl:'views/partials/fileOpenDialog.html'
    }
}])
    .directive('fileupload',  ['$parse', function ($parse) {/*From  http://plnkr.co/edit/yCKaKH?p=explore */
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        priority:maxPriority - 3,
        link: function( scope, elem, attrs, ctrl ) {
            function handleFileSelect(evt) {
                var files = evt.target.files; // FileList object
                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {
                    if (!((f.name.indexOf(".md") >= 0) ||
                        (f.name.indexOf(".txt") >= 0) ||
                        (f.name.indexOf(".htm") >= 0) ||
                        (f.name.indexOf(".html") >= 0))){
                        continue;
                    }

                    var reader = new FileReader();
                    // Closure to capture the file information.
                    reader.onload = (function(theFile) {
                        return function(e) {
                            scope.fileName = theFile.name;

                            var newStates = scope.checkCookieStates(
                                mm.core.cookieHelper.get(mm.cookieKeys.currentOpenFile),
                                theFile.name,
                                mm.core.cookieHelper.get(mm.cookieKeys.viewMode),
                                mm.core.cookieHelper.get(mm.cookieKeys.recentFiles),
                                mm.core.cookieHelper.get(mm.cookieKeys.isNewFile),
                                e.target.result);


                            mm.core.cookieHelper.set(mm.cookieKeys.isNewFile, newStates.isNewFile,
                                mm.notes.defaults.cache.expiryMilliSeconds);
                            mm.core.cookieHelper.set(mm.cookieKeys.recentFiles, newStates.recentFiles,
                                mm.notes.defaults.cache.expiryMilliSeconds);
                            mm.core.cookieHelper.set(mm.cookieKeys.currentOpenFile, newStates.currentOpenFile,
                                mm.notes.defaults.cache.expiryMilliSeconds);

                            scope.$parent.setViewingModeAndEditor(e.target.result);
                            /*if (newStates.isDiff) {
                                scope.$parent.setViewingModeAndEditor(e.target.result);
                            }*/
                        };
                    })(f);
                    reader.readAsBinaryString(f);
                    $('#fileOpenBtnClose').click();
                }
            }

            document.getElementById('files').addEventListener('change', handleFileSelect, false);
            $('#fileOpenBtn').on('click', function () {
                $('#files').click();

                return false;
            });
        }
    };
}])
