/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 10/6/12
 * Time: 10:42 AM
 * To change this template use File | Settings | File Templates.
 */


'use strict';

/* Filters */

angular.module('notesFilters', []).filter('checkmark', function() {
    return function(input) {
        return input ? '\u2713' : '\u2718';
    };
});

