/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 10/21/12
 * Time: 11:50 AM
 * To change this template use File | Settings | File Templates.
 */

notesApp.service('notesUserPreferencesModel', function () {
    var data = [
        {
            currentNote : "",
            preferences : {

            }
        }
    ];
    return {
        notes:function () {
            // This exposed private data
            return data;
        },
        saveNote:function (noteTitle) {
            // This is a public function that modifies private data


        },
        updateProperties: function(props){
            if (props !== undefined && props !== null){

            }
        },
        isSameAsCurrentNote:function (noteTitle) {
            // This is a public function that modifies private data
            return (noteTitle === currentNote);
        },
        deleteNote:function (id) {
            // This is a public function that modifies private data
        }
    };
})
