'use strict';

// Declare app level module which depends on filters, and services
var notesApp = angular.module('notesApp', ['notesFilters','EasyNotesComponents'])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/', {
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }]);
