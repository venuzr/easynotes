/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 10/17/12
 * Time: 5:45 PM
 * To change this template use File | Settings | File Templates.
 */

notesApp.service('noteServicesLibrary', function() {
    this.reverse = function(name) {
        return name.split("").reverse().join("");
    };
    this.setViewingModeAndEditor = function(scope, editorContents){
        function getViewtMode(scope){
          return mm.core.cookieHelper.get(mm.cookieKeys.viewMode) || mm.notes.defaults.displayMode;
        }
        var currentViewingMode = getViewtMode(scope);
        function updateEditorEventHandlerWithSchedule() {
            /***Ensure debouncing  http://benalman.com/code/projects/jquery-dotimeout/examples/debouncing/
             * Here do something in 0.3 second
             * */
            $.doTimeout('change', mm.notes.defaults.debounce.updateEditor, function(){
                /* This line needs to be here to ensure that the value used is the current one
                 * and not one used while creating the closure
                 * */
                currentViewingMode = getViewtMode(scope);
                 mm.core.EditorEvents.setViewingMode(currentViewingMode, true);
            });
        }


        mm.core.EditorEvents.getEditor().getSession().removeListener('change', updateEditorEventHandlerWithSchedule);
        if (editorContents !== undefined) {
            mm.core.EditorEvents.setEditorContents(editorContents);
        }
        mm.core.EditorEvents.getEditor().getSession().on('change', updateEditorEventHandlerWithSchedule);

        mm.core.EditorEvents.setViewingMode(currentViewingMode); //For first time when the page loads
    };


});