/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 10/22/12
 * Time: 10:42 AM
 * To change this template use File | Settings | File Templates.
 */

'use strict';
describe('Wrapper for all controllers', function () {

    beforeEach(function() {
        this.addMatchers({
            toBeNotNull: function() {
                return (this !== null);
            }
        });
    });

    describe('Controller: fileuploadCtrl', function () {

        // load the controller's module
        beforeEach(module('notesApp'));

        var ctrl,
            scope,
            noteServicesLibrary;

        // Initialize the controller and a mock scope
        beforeEach(inject(function ($controller) {
            scope = { $root:{}};
            noteServicesLibrary = { };
            ctrl = $controller('fileuploadCtrl', {
                $scope:scope,
                noteServicesLibrary:noteServicesLibrary
            });
        }));

        it('The controller is defined', function () {
            expect(ctrl).toBeDefined();
            expect(ctrl).not.toBeNull();
            expect(ctrl).toBeNotNull(); //Using custom matcher

            expect(scope.checkCookieStates).toBeNotNull();

        });

        it('When current open file and newly chosen file are the same, isDiff should be false', function () {
            var recentFiles = undefined;
            var isNewFile = true;
            var currentOpenFile = 'NewFile.md';

            var response = scope.checkCookieStates(currentOpenFile, currentOpenFile, 'e', recentFiles, isNewFile, undefined);

            expect(response).toBeDefined();
            expect(response.isDiff).toBeFalsy();
            expect(response.recentFiles).toBe(recentFiles);
            expect(response.currentOpenFile).toBe(currentOpenFile);
            expect(response.isNewFile).toBe(isNewFile);
        });

        it('When current open file and newly chosen file are the same with No previous file, isDiff should be false', function () {
            var recentFiles = undefined;
            var isNewFile = true;
            var currentOpenFile = 'NewFile.md';
            var chosenFile = 'ChosenFile.md'

            var response = scope.checkCookieStates(currentOpenFile, chosenFile, 'e', recentFiles, isNewFile, undefined);

            expect(response).toBeDefined();
            expect(response.isDiff).toBeTruthy();
            expect(response.recentFiles).toBe(chosenFile);
            expect(response.isNewFile).toBeFalsy();
            expect(response.currentOpenFile).toBe(chosenFile);
        });

        it('When current open file and newly chosen file are the same with existing previous file, isDiff should be false', function () {
            var recentFiles = 'FirstFile.md';
            var isNewFile = true;
            var currentOpenFile = 'NewFile.md';
            var chosenFile = 'ChosenFile.md'

            var response = scope.checkCookieStates(currentOpenFile, chosenFile, 'e', recentFiles, isNewFile, undefined);

            expect(response).toBeDefined();
            expect(response.isDiff).toBeTruthy();
            expect(response.recentFiles).toBe(recentFiles + mm.notes.defaults.parser.delimiter + chosenFile);
            expect(response.isNewFile).toBeFalsy();
            expect(response.currentOpenFile).toBe(chosenFile);
        });

        it('When current open file and newly chosen file are the same with existing recent files which contain current file, isDiff should be false', function () {
            var chosenFile = 'ChosenFile.md';
            var recentFiles = 'FirstFile.md' + mm.notes.defaults.parser.delimiter + chosenFile;
            var isNewFile = true;
            var currentOpenFile = 'NewFile.md';

            var response = scope.checkCookieStates(currentOpenFile, chosenFile, 'e', recentFiles, isNewFile, undefined);

            expect(response).toBeDefined();
            expect(response.isDiff).toBeTruthy();
            expect(response.recentFiles).toBe(recentFiles);
            expect(response.isNewFile).toBeFalsy();
            expect(response.currentOpenFile).toBe(chosenFile);
        });

    });

});

