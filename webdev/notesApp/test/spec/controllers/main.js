'use strict';

describe('Controller: MainCtrl', function() {

    // load the controller's module
    beforeEach(module('notesApp'));

    var MainCtrl,
        scope,
        cookies;

    // Initialize the controller and a mock scope
    beforeEach(inject(function($controller) {
        scope = { $root : {}};
        cookies = {};
        MainCtrl = $controller('MainCtrl', {
            $scope: scope,
            //$cookies: cookies
        });
    }));

    it('should attach a list of awesomeThings to the scope', function() {
        expect(scope.awesomeThings.length).toBe(3);
    });
});
