/**
 * Created with IntelliJ IDEA.
 * User: venuzr
 * Date: 10/22/12
 * Time: 10:20 AM
 * To change this template use File | Settings | File Templates.
 */

describe('Core tests', function() {

    beforeEach(function() {


    });

    afterEach(function() {

    });


    describe('String comparison tests- stringUtils.js', function() {
        it('Positive test for Pattern match', function() {
            expect(mm.core.StringUtils.searchForSubString('FirstFile.md$$$SecondFile.md$$$ThirdFile.md', 'SecondFile.md', '$$$')).toBe(true);
            expect(mm.core.StringUtils.searchForSubString('FirstFile.md$$$SecondFile.md$$$ThirdFile.md', 'FirstFile.md', '$$$')).toBe(true);
            expect(mm.core.StringUtils.searchForSubString('FirstFile.md$$$SecondFile.md$$$ThirdFile.md', 'ThirdFile.md', '$$$')).toBe(true);
        });

        it('Negative test for Pattern match', function() {
            expect(mm.core.StringUtils.searchForSubString('FirstFile.md$$$SecondFile.md$$$ThirdFile.md', 'SeconFile.md', '$$$')).toBe(false); //Typo
            expect(mm.core.StringUtils.searchForSubString('FirstFile.md$$$SecondFile.md$$$ThirdFile.md', 'ZerothFile.md', '$$$')).toBe(false); //Non existant
            expect(mm.core.StringUtils.searchForSubString('FirstFile.md$$$SecondFile.md$$$ThirdFile.md', 'ThirdFil.md', '$$$')).toBe(false); //Typo again
        });

        it('Negative test for  incorrect Pattern match', function() {
            expect(mm.core.StringUtils.searchForSubString('FirstFile.md$$$SecondFile.md$$$ThirdFile.md', 'SecondFile.md', '$$')).toBe(false);
            expect(mm.core.StringUtils.searchForSubString('FirstFile.md$$$SecondFile.md$$$ThirdFile.md', 'FirstFile.md', '$$%')).toBe(false);
            expect(mm.core.StringUtils.searchForSubString('FirstFile.md$$$SecondFile.md$$$ThirdFile.md', 'ThirdFile.md', '')).toBe(false);
            expect(mm.core.StringUtils.searchForSubString('FirstFile.md$$$SecondFile.md$$$ThirdFile.md', 'ThirdFile.md', 'XYZ')).toBe(false);
        });


    });

    describe('stickyLayout tests - Try to mock the interactions with the DOM elements', function() {


    });
});