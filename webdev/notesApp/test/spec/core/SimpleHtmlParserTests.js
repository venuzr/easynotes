
describe('SimpleHtmlParser tests', function() {

    beforeEach(function() {

    });

    afterEach(function() {

    });

    describe('Should be able to handle empty html content', function() {
        it('Empty html', function() {
            var drawTree = mm.core.parser.SimpleHtmlParser.parse('<div/>');
            console.log(drawTree)//prints output to developer tools console
           // dump(drawTree) //prints output to commandline
            //use console.log(drawTree) to print output to developer tools or
            expect(drawTree).not.toBeNull();
            expect(drawTree.length).toBe(0); //Empty array
        });
    });
    describe('Should be able to handle Nonempty html content and load fixtures', function() {
        beforeEach(function(){
            jasmine.getFixtures().fixturesPath = '/views/fixtures/HtmlParser';
        });

        it('Non empty html with just a focus on root node values', function() {
            loadFixtures('SimpleHtmlParser_firstNonEmpty.html');
            expect($('#preview')).toHaveClass('editorial_settings');


            // var drawTree = mm.core.SimpleHtmlParser.parse($('#preview'));
            // expect(drawTree).not.toBeNull();
            // expect(drawTree.length).toBe(1);
            // expect(drawTree[0].name).toBe('This is initial')
            // expect(drawTree[0]).not.toBeNull();
            // expect(drawTree[0].level).toBe(1);
            // expect(drawTree[0].strength).toBe(1);
            // expect(drawTree[0].p2CrelationShip).not.toBeDefined();
            // expect(drawTree[0].c2PrelationShip).not.toBeDefined();

            // expect(drawTree[0].hyperlink).toBe('http://www.google.com');
            // expect(drawTree[0].p2CrelationShip).not.toBeDefined();
            // expect(drawTree[0].c2PrelationShip).not.toBeDefined();
        });



        /*******Need to figure out how to look at the object in console.log ***/
    //     it('Comprehensive html with single root level(H1) block', function() {
    //         loadFixtures('comprehensive_response.html');
    //         expect($('#preview')).toHaveClass('editorial_settings');

    //         var drawTree = mm.core.SimpleHtmlParser.parse($('#preview'));
    //         expect(drawTree).not.toBeNull();
    //         expect(drawTree.length).toBe(1);
    //         expect(drawTree[0]).not.toBeNull();
    //         expect(drawTree[0].name).toBe('jawdropping Asia trip')
    //         expect(drawTree[0].level).toBe(1);
    //         expect(drawTree[0].strength).toBe(1);
    //         expect(drawTree[0].parent).toBeNull();
    //         expect(drawTree[0].children).not.toBeNull(); ///Only this is different from above
    //         expect(drawTree[0].hyperlink).toBe('http://www.google.com');
    //         expect(drawTree[0].p2CrelationShip).not.toBeDefined();
    //         expect(drawTree[0].c2PrelationShip).not.toBeDefined();
    //         expect(drawTree[0].children.length).toBe(3)
    //         expect(drawTree[0].children[0].p2CrelationShip).toBe('hectic part');
    //         expect(drawTree[0].children[0].c2PrelationShip).toBe('Family affair');


    //         expect(drawTree[0].children[1].p2CrelationShip).toBe('relaxing part');
    //         expect(drawTree[0].children[1].c2PrelationShip).toBe('time management')
    //     });



    //     it('Comprehensive html with single root level(H1) block', function() {
    //         loadFixtures('comprehensive_response_with_multiple_H1_blocks.html');
    //         expect($('#preview')).toHaveClass('editorial_settings');

    //         var drawTree = mm.core.SimpleHtmlParser.parse($('#preview'));
    //         expect(drawTree).not.toBeNull();
    //         expect(drawTree.length).toBe(3);
    //         expect(drawTree[0]).not.toBeNull();
    //         expect(drawTree[0].level).toBe(1);
    //         expect(drawTree[0].strength).toBe(1);

    //         expect(drawTree[0].name).toBe('Taiwan leg holl');
    //         expect(drawTree[0].parent).toBeNull();
    //         expect(drawTree[0].children).not.toBeNull(); ///Only this is different from above
    //         expect(drawTree[0].hyperlink).toBe('http://www.google.com');
    //         expect(drawTree[0].p2CrelationShip).toBe('hectic part')
    //         expect(drawTree[0].c2PrelationShip).toBe('Family affair');
    //         expect(drawTree[0].children.length).toBe(2)
    //         expect(drawTree[0].children[0].name).toBe('Taipei');
    //         expect(drawTree[0].children[0].p2CrelationShip).not.toBeDefined();
    //         expect(drawTree[0].children[0].c2PrelationShip).not.toBeDefined();
    //         expect(drawTree[0].children[1].name).toBe('Taroka gorge');
    //         expect(drawTree[0].children[1].p2CrelationShip).not.toBeDefined();
    //         expect(drawTree[0].children[1].c2PrelationShip).not.toBeDefined();

    //         expect(drawTree[1].name).toBe('Thailand leg');
    //         expect(drawTree[1].parent).toBeNull();
    //         expect(drawTree[1].children).not.toBeNull(); ///Only this is different from above
    //         expect(drawTree[1].hyperlink).toBe('http://www.google.com');
    //         //expect(drawTree[0].p2CrelationShip).toBe('hectic part')
    //         //expect(drawTree[0].c2PrelationShip).toBe('Family affair');


    //     });
    // });


    // describe('Should be able to clean html for custom tags', function(){
    //     it('Remove single custom Node tags', function() {
    //         var prefixText = 'Some initial text here';
    //         var midSuffixText = 'some ending here';

    //         var text = prefixText + mm.notes.defaults.parser.nodeTag + midSuffixText;

    //         var cleanText = mm.core.SimpleHtmlParser.getCleanedHtml(text);
    //         expect(cleanText).toBe(prefixText + midSuffixText);
    //     });

    //     it('Remove multiple Node tags', function() {
    //         var prefixText = 'Some initial text here';
    //         var midSuffixText = 'some ending here';

    //         var text =  mm.notes.defaults.parser.nodeTag + prefixText +
    //                     mm.notes.defaults.parser.nodeTag + midSuffixText +
    //                     mm.notes.defaults.parser.nodeTag;

    //         var cleanText = mm.core.SimpleHtmlParser.getCleanedHtml(text);
    //         expect(cleanText).toBe(prefixText + midSuffixText);
    //     });
    //     it('Remove multiple parent to child tags', function() {
    //         var prefixText = 'Some initial text here';
    //         var midSuffixText = 'some ending here';

    //         var text = mm.notes.defaults.parser.p2CrelationTag + prefixText +
    //                     mm.notes.defaults.parser.p2CrelationTag + midSuffixText +
    //                     mm.notes.defaults.parser.p2CrelationTag;

    //         var cleanText = mm.core.SimpleHtmlParser.getCleanedHtml(text);
    //         expect(cleanText).toBe(prefixText + midSuffixText);
    //     });
    //     it('Remove multiple child to parent tags', function() {
    //         var prefixText = 'Some initial text here';
    //         var midSuffixText = 'some ending here';

    //         var text = mm.notes.defaults.parser.c2PrelationTag + prefixText +
    //             mm.notes.defaults.parser.c2PrelationTag + midSuffixText +
    //             mm.notes.defaults.parser.c2PrelationTag;

    //         var cleanText = mm.core.SimpleHtmlParser.getCleanedHtml(text);
    //         expect(cleanText).toBe(prefixText + midSuffixText);
    //     });
    //     it('Remove All combinations of custom tags tags', function() {
    //         var prefixText = 'Some initial text here';
    //         var midSuffixText = 'some ending here';
    //         var finalSuffixText = 'final suffixText';

    //         var text = prefixText + mm.notes.defaults.parser.nodeTag + midSuffixText +
    //             mm.notes.defaults.parser.p2CrelationTag + finalSuffixText +
    //             mm.notes.defaults.parser.c2PrelationTag +
    //             mm.notes.defaults.parser.c2PrelationTag +
    //             mm.notes.defaults.parser.c2PrelationTag +
    //             mm.notes.defaults.parser.nodeTag +
    //             mm.notes.defaults.parser.nodeTag +
    //             mm.notes.defaults.parser.p2CrelationTag;

    //         var cleanText = mm.core.SimpleHtmlParser.getCleanedHtml(text);
    //         expect(cleanText).toBe(prefixText + midSuffixText + finalSuffixText);
    //     });

    });

    // describe('Should be able to parse node content', function() {
    //     it('Get empty object for non existent level', function() {
    //         var drawTree = mm.core.SimpleHtmlParser.parse('<div/>');
    //         console.log(drawTree)//prints output to developer tools console
    //         // dump(drawTree) //prints output to commandline
    //         //use console.log(drawTree) to print output to developer tools or
    //         expect(drawTree).not.toBeNull();
    //         expect(drawTree.length).toBe(0); //Empty array
    //     });
    // });

});